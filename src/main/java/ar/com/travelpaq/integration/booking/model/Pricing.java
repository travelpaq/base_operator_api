package ar.com.travelpaq.integration.booking.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Pricing {

    @JsonProperty("total")
    private Float total;

    @JsonProperty("commission_amount")
    private Float commissionAmount;

    @JsonProperty("commissionable_price")
    private Float commissionablePrice;

    @JsonProperty("tourism_taxes")
    private List<Tax> tourismTaxes;

    @JsonProperty("fiscal_taxes")
    private List<Tax> fiscalTaxes;

    @JsonProperty("non_commissionable_service")
    private List<Tax> nonCommissionableService;

    public Pricing(Float total, Float commission_amount, Float commissionablePrice, List<Tax> tourismTaxes, List<Tax> fiscalTaxes, List<Tax> nonCommissionableService) {
        this.total = total;
        this.commissionAmount = commission_amount;
        this.commissionablePrice = commissionablePrice;
        this.tourismTaxes = tourismTaxes;
        this.fiscalTaxes = fiscalTaxes;
        this.nonCommissionableService = nonCommissionableService;
    }

    public Pricing() {
        super();
    }

    @JsonProperty("total")
    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    @JsonProperty("commission_amount")
    public Float getCommissionAmount() {
        return commissionAmount;
    }

    public void setCommissionAmount(Float commissionAmount) {
        this.commissionAmount = commissionAmount;
    }

    @JsonProperty("commissionable_price")
    public Float getCommissionablePrice() {
        return commissionablePrice;
    }

    public void setCommissionablePrice(Float commissionablePrice) {
        this.commissionablePrice = commissionablePrice;
    }

    @JsonProperty("tourism_taxes")
    public List<Tax> getTourismTaxes() {
        return tourismTaxes;
    }

    public void setTourismTaxes(List<Tax> tourismTaxes) {
        this.tourismTaxes = tourismTaxes;
    }

    @JsonProperty("fiscal_taxes")
    public List<Tax> getFiscalTaxes() {
        return fiscalTaxes;
    }

    public void setFiscalTaxes(List<Tax> fiscalTaxes) {
        this.fiscalTaxes = fiscalTaxes;
    }

    @JsonProperty("non_commissionable_service")
    public List<Tax> getNonCommissionableService() {
        return nonCommissionableService;
    }

    public void setNonCommissionableService(List<Tax> nonCommissionableService) {
        this.nonCommissionableService = nonCommissionableService;
    }
}

