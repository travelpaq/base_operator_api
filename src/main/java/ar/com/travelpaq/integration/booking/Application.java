package ar.com.travelpaq.integration.booking;

import ar.com.travelpaq.integration.commons.connection.LambdaInput;
import ar.com.travelpaq.integration.commons.connection.LambdaOutput;
import com.amazonaws.services.lambda.runtime.Context;
import java.util.logging.Logger;

public class Application {
    private static final Logger LOGGER = Logger.getLogger(Application.class.getName());

    public static LambdaOutput manageRequest(LambdaInput lambdaInput, Context context, BaseBooker booker) {
        String httpMethod = lambdaInput.getHttpMethod();
        LambdaOutput response = null;

        switch (httpMethod) {
            case "POST":
                LOGGER.info("Calling method 'Booker.book()'");
                response = new LambdaOutput(200, "Calling method 'Booker.book()'");
                // Booking responseBooking = booker.book();
                // response = new LambdaOutput(HttpStatus.OK.value(), ObjectUtils.convertValue(responseBooking));
                break;
            case "PUT":
                String status = booker.getBooking().getStatus();
                switch (status) {
                    case "CONFIRMED":
                        LOGGER.info("Calling method 'Booker.confirmBooking()'");
                        response = new LambdaOutput(200, "Calling method 'Booker.confirmBooking()'");
                        // Booking responseBooking = booker.confirmBooking();
                        // response = new LambdaOutput(HttpStatus.OK.value(), ObjectUtils.convertValue(responseBooking));
                        break;
                    case "CANCELED":
                        LOGGER.info("Calling method 'Booker.cancelBooking()'");
                        response = new LambdaOutput(200, "Calling method 'Booker.cancelBooking()'");
                        // Booking responseBooking = booker.cancelBooking();
                        // response = new LambdaOutput(HttpStatus.OK.value(), ObjectUtils.convertValue(responseBooking));
                        break;
                    default:
                        LOGGER.warning("Booking status must be 'CANCELED' or 'CONFIRMED'");
                        response = new LambdaOutput(400, "Booking status must be 'CANCELED' or 'CONFIRMED'");
                        break;
                }
                break;
            default:
                LOGGER.warning("Request method must be 'POST' or 'PUT'");
                response = new LambdaOutput(400, "Request method must be 'POST' or 'PUT'");
                break;
        }
        return response;
    }
}
