package ar.com.travelpaq.integration.booking.model;

import ar.com.travelpaq.integration.commons.model.domain.Package;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Booking {

    private String status;

    private Integer id;

    private String externalId;

    private String packageId;

    private String contactPhone;

    private String message;

    private String observations;

    private Integer agencyId;

    private List<List<Passenger>> rooms;

    private Package _package;

    private Pricing pricing;



    public Booking(String status, Integer id, String externalId, String packageId, String contactPhone, List<List<Passenger>> rooms, Package _package, Pricing pricing, Integer agencyId) {
        this.status = status;
        this.id = id;
        this.externalId = externalId;
        this.packageId = packageId;
        this.contactPhone = contactPhone;
        this.rooms = rooms;
        this._package = _package;
        this.pricing = pricing;
        this.agencyId = agencyId;
    }

    public Booking() {
        super();
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("external_id")
    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @JsonProperty("package_id")
    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    @JsonProperty("contact_phone")
    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("observations")
    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    @JsonProperty("rooms")
    public List<List<Passenger>> getRooms() {
        return rooms;
    }

    public void setRooms(List<List<Passenger>> rooms) {
        this.rooms = rooms;
    }

    @JsonProperty("package")
    public Package get_package() {
        return _package;
    }

    public void set_package(Package _package) {
        this._package = _package;
    }

    @JsonProperty("pricing")
    public Pricing getPricing() {
        return pricing;
    }

    public void setPricing(Pricing pricing) {
        this.pricing = pricing;
    }

    @JsonProperty("agency_id")
    public Integer getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(Integer agencyId) {
        this.agencyId = agencyId;
    }
}
