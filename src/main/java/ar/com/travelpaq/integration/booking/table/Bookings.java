package ar.com.travelpaq.integration.booking.table;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "bookings")
public class Bookings implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id", updatable = false)
    private Integer id;

    @Column(name = "operator_id", updatable = false, nullable=true)
    private Integer operator_id;

    @Column(name = "agency_id", updatable = false, nullable=true)
    private Integer agency_id;

    @Column(name = "external_id")
    private String external_id;

    @Column(name = "status")
    private String status;

    @Column(name = "created", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Column(name = "message_error", updatable = false, nullable=true)
    private String message_error;

    @Column(name = "markup", updatable = false, nullable=true)
    private Float markup;

    @Column(name = "type_change", updatable = false, nullable=true)
    private Float type_change;

    @Column(name = "sale_agency_id", updatable = false, nullable=true)
    private Integer sale_agency_id;

    @Column(name = "sale_operator_id", updatable = false, nullable=true)
    private Integer sale_operator_id;

    @Column(name = "package_id",updatable = false, nullable=true)
    private String package_id;

    @Column(name = "agency_comission", updatable = false, nullable=true)
    private Float agency_comission;

    @Column(name = "contact_phone", updatable = false, nullable=true)
    private String contact_phone;

    @Column(name = "percentage_tp_operator", nullable=true)
    private Float percentage_tp_operator;

    @Column(name = "percentage_tp_agency", nullable=true)
    private Float percentage_tp_agency;

    @Column(name = "label", updatable = false, nullable=true)
    private String label;

    @OneToMany(mappedBy="bookings", targetEntity = Pricings.class)
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private Set<Pricings> pricings = new HashSet(0);

    @OneToMany(mappedBy="bookings", targetEntity = Passengers.class)
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private Set<Passengers> passengers = new HashSet(0);


    public Bookings() {
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOperator_id() {
        return operator_id;
    }

    public void setOperator_id(Integer operator_id) {
        this.operator_id = operator_id;
    }

    public Integer getAgency_id() {
        return agency_id;
    }

    public void setAgency_id(Integer agency_id) {
        this.agency_id = agency_id;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getMessage_error() {
        return message_error;
    }

    public void setMessage_error(String message_error) {
        this.message_error = message_error;
    }

    public Float getMarkup() {
        return markup;
    }

    public void setMarkup(Float markup) {
        this.markup = markup;
    }

    public Float getType_change() {
        return type_change;
    }

    public void setType_change(Float type_change) {
        this.type_change = type_change;
    }

    public Integer getSale_agency_id() {
        return sale_agency_id;
    }

    public void setSale_agency_id(Integer sale_agency_id) {
        this.sale_agency_id = sale_agency_id;
    }

    public Integer getSale_operator_id() {
        return sale_operator_id;
    }

    public void setSale_operator_id(Integer sale_operator_id) {
        this.sale_operator_id = sale_operator_id;
    }

    public String getPackage_id() {
        return package_id;
    }

    public void setPackage_id(String package_id) {
        this.package_id = package_id;
    }

    public Float getAgency_comission() {
        return agency_comission;
    }

    public void setAgency_comission(Float agency_comission) {
        this.agency_comission = agency_comission;
    }

    public String getContact_phone() {
        return contact_phone;
    }

    public void setContact_phone(String contact_phone) {
        this.contact_phone = contact_phone;
    }

    public Float getPercentage_tp_operator() {
        return percentage_tp_operator;
    }

    public void setPercentage_tp_operator(Float percentage_tp_operator) {
        this.percentage_tp_operator = percentage_tp_operator;
    }

    public Float getPercentage_tp_agency() {
        return percentage_tp_agency;
    }

    public void setPercentage_tp_agency(Float percentage_tp_agency) {
        this.percentage_tp_agency = percentage_tp_agency;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Set<Passengers> getPassengers() {
        return passengers;
    }

    public void setPassengers(Set<Passengers> passengers) {
        if (this.passengers == null) {
            this.passengers = passengers;
        } else {
            this.passengers.clear();
            this.passengers.addAll(passengers);
        }
    }

    public Set<Pricings> getPricings() {
        return pricings;
    }

    public void setPricings(Set<Pricings> pricings) {
        if (this.pricings == null) {
            this.pricings = pricings;
        } else {
            this.pricings.clear();
            this.pricings.addAll(pricings);
        }
    }
}
