package ar.com.travelpaq.integration.booking.table;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "booking_taxes")
public class Taxes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id")
    private Integer id;

    @Column(name ="rate")
    private Float rate;

    @Column(name ="base")
    private Float base;

    @Column(name ="amount")
    private Float amount;

    @Column(name ="description")
    private String description;

    @Column(name ="kind")
    private String kind;

    @Column(name ="created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @ManyToOne
    @JoinColumn(name="pricing_id")
    private Pricings pricings;

    public Taxes() {
    }

    public Taxes(Float rate, Float base, Float amount, String description, String kind, Date created, Pricings pricings) {
        this.rate = rate;
        this.base = base;
        this.amount = amount;
        this.description = description;
        this.kind = kind;
        this.created = created;
        this.pricings = pricings;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public Float getBase() {
        return base;
    }

    public void setBase(Float base) {
        this.base = base;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Pricings getPricings() {
        return pricings;
    }

    public void setPricings(Pricings pricings) {
        this.pricings = pricings;
    }
}
