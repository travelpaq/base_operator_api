package ar.com.travelpaq.integration.booking.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Tax {

    @JsonProperty("rate")
    private Float rate;

    @JsonProperty("base")
    private Float base;

    @JsonProperty("amount")
    private Float amount;

    @JsonProperty("description")
    private String description;

    public Tax(Float rate, Float base, Float amount, String description) {
        this.rate = rate;
        this.base = base;
        this.amount = amount;
        this.description = description;
    }

    public Tax() {
        super();
    }

    @JsonProperty("rate")
    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    @JsonProperty("base")
    public Float getBase() {
        return base;
    }

    public void setBase(Float base) {
        this.base = base;
    }

    @JsonProperty("amount")
    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
