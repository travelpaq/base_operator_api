package ar.com.travelpaq.integration.booking;

import ar.com.travelpaq.integration.booking.model.Booking;
import ar.com.travelpaq.integration.booking.model.Pricing;
import ar.com.travelpaq.integration.commons.configuration.ExecutionMode;
import ar.com.travelpaq.integration.commons.connection.ConnectionBookData;
import ar.com.travelpaq.integration.commons.connection.Integration;
import ar.com.travelpaq.integration.commons.model.Persistence;
import ar.com.travelpaq.integration.commons.utils.MessagesList;

import java.io.IOException;

public abstract class BaseBooker {

    protected Booking booking;
    protected Object bookingResponse;
    protected Integer operatorId;
    protected Integer agencyId;
    protected ExecutionMode executionMode;
    protected ConnectionBookData connectionBookData;


    public BaseBooker(Booking booking, ExecutionMode executionMode) throws IOException {
        this.booking = booking;
        this.operatorId = booking.get_package().getCompany().getId();
        this.agencyId = booking.getAgencyId();
        this.executionMode = executionMode;
        connectionBookData = Integration.getBookConnectionData(agencyId,operatorId,executionMode);
    }

    public Booking book(){

        if(!buildFare()){
            booking.setStatus(BookingStatus.ERROR);
            booking.setMessage(MessagesList.BOOKING_BUILD_FARE_ERROR);
            return booking;
        }


        if(!buildRooms()){
            booking.setStatus(BookingStatus.ERROR);
            booking.setMessage(MessagesList.BOOKING_BUILD_ROOM);
            return booking;
        }

        if(!prevStep()){
            booking.setStatus(BookingStatus.ERROR);
            booking.setMessage(MessagesList.BOOKING_PREV_STEP);
            return booking;
        }

        bookingResponse = supplierBookPackage();

        if(bookingResponse == null){
            booking.setStatus(BookingStatus.ERROR);
            booking.setMessage(MessagesList.BOOKING_SUPPLIER_BOOKING);
            return booking;
        }


        Pricing pricing = getPricing();

        if(pricing == null)
            booking.setMessage(MessagesList.BOOKING_GET_PRICING);

        booking.setPricing(pricing);

        booking.setStatus(BookingStatus.WAITING);

        if(!Persistence.saveBooking(booking, executionMode, connectionBookData)){
            booking.setMessage(MessagesList.BOOKING_GET_PRICING);
            return booking;
        }

        return booking;
    }

    public void confirmBooking(){

        if(supplierConfirmBooking())
            booking.setStatus(BookingStatus.CONFIRMED);
        else
            booking.setStatus(BookingStatus.CONFIRMING);

        Persistence.updateBooking(booking, executionMode, connectionBookData);
    }

    public void cancelBooking(){

        if(supplierCancelBooking())
            booking.setStatus(BookingStatus.CANCELED);
        else
            booking.setStatus(BookingStatus.CANCELING);

        Persistence.updateBooking(booking, executionMode, connectionBookData);
    }

    public Booking getBooking() {
        return booking;
    }

    public abstract Boolean buildFare();

    public abstract Boolean prevStep();

    public abstract Boolean buildRooms();

    public abstract Object supplierBookPackage();

    public abstract Boolean supplierConfirmBooking();

    public abstract Boolean supplierCancelBooking();

    public abstract Pricing getPricing();
}
