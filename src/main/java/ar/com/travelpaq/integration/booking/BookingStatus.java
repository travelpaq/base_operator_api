package ar.com.travelpaq.integration.booking;

public class BookingStatus {
    public static String WAITING = "WAITING";
    public static String CONFIRMED = "CONFIRMED";
    public static String CANCELED = "CANCELED";
    public static String CANCELING = "CANCELING";
    public static String CONFIRMING = "CONFIRMING";
    public static String ERROR = "ERROR";
    public static String EXPIRED = "EXPIRED";
    public static String REQUIRED = "REQUIRED";
    public static String CREATING = "CREATING";
}
