package ar.com.travelpaq.integration.booking.table;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "passengers")
public class Passengers implements java.io.Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id")
    private Integer id;

    @Column(name ="name")
    private String name;

    @Column(name ="surname")
    private String surname;

    @Column(name ="kind_doc")
    private String kind_doc;

    @Column(name ="num_doc")
    private String num_doc;

    @Column(name ="gender")
    private String gender;

    @Column(name ="birthday")
    @Temporal(TemporalType.DATE)
    private Date birthday;

    @Column(name ="residence")
    private String residence;

    @Column(name ="nationality")
    private String nationality;

    @Column(name ="mail")
    private String mail;

    @Column(name ="expired_date_passport")
    @Temporal(TemporalType.DATE)
    private Date expired_date_passport;

    @Column(name ="created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Column(name ="room")
    private Integer room;

    @ManyToOne
    @JoinColumn(name="booking_id", nullable=false)
    private Bookings bookings;


    public Passengers() {
    }

    public Passengers(String name, String surname, String kind_doc, String num_doc, String gender, Date birthday,
                      String residence, String nationality, String mail, Date expired_date_passport,
                      Date created, Integer room, Bookings bookings) {
        this.name = name;
        this.surname = surname;
        this.kind_doc = kind_doc;
        this.num_doc = num_doc;
        this.gender = gender;
        this.birthday = birthday;
        this.residence = residence;
        this.nationality = nationality;
        this.mail = mail;
        this.expired_date_passport = expired_date_passport;
        this.created = created;
        this.room = room;
        this.bookings = bookings;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getKind_doc() {
        return kind_doc;
    }

    public void setKind_doc(String kind_doc) {
        this.kind_doc = kind_doc;
    }

    public String getNum_doc() {
        return num_doc;
    }

    public void setNum_doc(String num_doc) {
        this.num_doc = num_doc;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getResidence() {
        return residence;
    }

    public void setResidence(String residence) {
        this.residence = residence;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Date getExpired_date_passport() {
        return expired_date_passport;
    }

    public void setExpired_date_passport(Date expired_date_passport) {
        this.expired_date_passport = expired_date_passport;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Integer getRoom() {
        return room;
    }

    public void setRoom(Integer room) {
        this.room = room;
    }

    public Bookings getBookings() {
        return bookings;
    }

    public void setBookings(Bookings bookings) {
        this.bookings = bookings;
    }
}
