package ar.com.travelpaq.integration.booking.table;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "pricings")
public class Pricings {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id")
    private Integer id;

    @Column(name ="total")
    private Float total;

    @Column(name ="commission_amount")
    private Float commission_amount;

    @Column(name ="commissionable_price")
    private Float commissionable_price;

    @Column(name ="created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @ManyToOne
    @JoinColumn(name="booking_id", nullable=false)
    private Bookings bookings;

    @OneToMany(mappedBy="pricings", targetEntity = ar.com.travelpaq.integration.booking.table.Taxes.class)
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private Set<Taxes> taxes = new HashSet(0);

    public Pricings() {
    }

    public Pricings(Float total, Float commission_amount, Float commissionable_price, Date created, Bookings bookings,
                    Set<Taxes> taxes) {
        this.total = total;
        this.commission_amount = commission_amount;
        this.commissionable_price = commissionable_price;
        this.created = created;
        this.bookings = bookings;
        this.taxes = taxes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public Float getCommission_amount() {
        return commission_amount;
    }

    public void setCommission_amount(Float commission_amount) {
        this.commission_amount = commission_amount;
    }

    public Float getCommissionable_price() {
        return commissionable_price;
    }

    public void setCommissionable_price(Float commissionable_price) {
        this.commissionable_price = commissionable_price;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Bookings getBookings() {
        return bookings;
    }

    public void setBookings(Bookings bookings) {
        this.bookings = bookings;
    }

    public Set<Taxes> getTaxes() {
        return taxes;
    }

    public void setTaxes(Set<Taxes> taxes) {
        if (this.taxes == null) {
            this.taxes = taxes;
        } else {
            this.taxes.clear();
            this.taxes.addAll(taxes);
        }
    }
}
