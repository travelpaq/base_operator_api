package ar.com.travelpaq.integration.booking.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Passenger {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("surname")
    private String surname;

    @JsonProperty("kind_doc")
    private String kindDoc;

    @JsonProperty("num_doc")
    private String numDoc;

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("birthdate")
    private String birthdate;

    @JsonProperty("residence")
    private String residence;

    @JsonProperty("nationality")
    private String nationality;

    @JsonProperty("mail")
    private String mail;

    @JsonProperty("expiration_passport_date")
    private String expirationPassportDate;


    public Passenger(Integer id, String name, String surname, String kindDoc, String numDoc, String gender,
                     String birthdate, String residence, String nationality, String mail, String expirationPassportDate) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.kindDoc = kindDoc;
        this.numDoc = numDoc;
        this.gender = gender;
        this.birthdate = birthdate;
        this.residence = residence;
        this.nationality = nationality;
        this.mail = mail;
        this.expirationPassportDate = expirationPassportDate;
    }

    public Passenger(){
        super();
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("surname")
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @JsonProperty("kind_doc")
    public String getKindDoc() {
        return kindDoc;
    }

    public void setKindDoc(String kindDoc) {
        this.kindDoc = kindDoc;
    }

    @JsonProperty("num_doc")
    public String getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    @JsonProperty("gender")
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @JsonProperty("birthdate")
    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    @JsonProperty("residence")
    public String getResidence() {
        return residence;
    }

    public void setResidence(String residence) {
        this.residence = residence;
    }

    @JsonProperty("nationality")
    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @JsonProperty("mail")
    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @JsonProperty("expiration_passport_date")
    public String getExpirationPassportDate() {
        return expirationPassportDate;
    }

    public void setExpirationPassportDate(String expirationPassportDate) {
        this.expirationPassportDate = expirationPassportDate;
    }

}
