package ar.com.travelpaq.integration.mapper;

import java.io.*;
import java.util.HashMap;

import ar.com.travelpaq.integration.commons.auth.Authentication;
import ar.com.travelpaq.integration.commons.connection.LambdaInput;
import ar.com.travelpaq.integration.commons.connection.LambdaOutput;
import ar.com.travelpaq.integration.commons.utils.ObjectUtils;
import ar.com.travelpaq.integration.commons.utils.QueueMessage;
import ar.com.travelpaq.integration.commons.utils.QueueName;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import org.apache.http.HttpStatus;
import org.json.JSONObject;
import ar.com.travelpaq.integration.commons.utils.QueueService;
import javax.xml.bind.JAXBException;

public abstract class BaseMapper {

    /**
     * Metodo abstracto que debe ser implementado en los mappers particulares para cada operador
     *
     */
    public abstract String startMapping(String unmappedData, String operatorId) throws JAXBException;

    /*protected LambdaOutput manageRequest(LambdaInput input, Context context) {
        // Parsear input y extraer message receipt y nombre de cola
        QueueMessage incomingMessage = ObjectUtils.convertValue(input.getBody(), QueueMessage.class);

        if(!Authentication.checkSuperAdmin(input.getHeaders().get("TP-AUTH")))
            return new LambdaOutput(HttpStatus.SC_UNAUTHORIZED, "Authentication token is wrong.");

        LambdaLogger logger       = context.getLogger();
        QueueService queueService = new QueueService(incomingMessage.getRegionQueueName());
        LambdaOutput output;


        String queueFromUrl  = queueService.getQueueURL(incomingMessage.getQueueName());
        String queueToUrl    = queueService.getQueueURL(QueueName.STANDARDIZATION.getQueueName());
        try {
            if(incomingMessage != null) {
                // Llamamos al metodo que realiza el mapeo en la clase hija
                logger.log("Starting mapping process. Operator ID: " + incomingMessage.getOperatorId() + ". From queue: " + incomingMessage.getQueueName());
                String result = this.startMapping(incomingMessage.getData(), incomingMessage.getOperatorId());

                // Construimos response de éxito
                output = new LambdaOutput(HttpStatus.SC_OK, "");

                // Enviamos resultado del mapeo a la cola de estandariazcion
                QueueMessage message = new QueueMessage(result, incomingMessage.getOperatorId(), QueueName.STANDARDIZATION.getQueueName(),QueueName.STANDARDIZATION.getQueueRegion());
                HashMap<String, String> sendMsgResult = queueService.sendMessage(queueToUrl, ObjectUtils.convertValue(message), 0);
                logger.log("Mapping result sent to standardization queue. Message ID: " + sendMsgResult.get("MessageId"));

                // Eliminamos el mensaje ya procesado de la cola de origen
                queueService.deleteMessage(queueFromUrl, incomingMessage.getReceiptHandle());
                logger.log("Deleting message with receipt " + incomingMessage.getReceiptHandle() + " from queue with URL " + queueFromUrl + "\n");
            } else {
                output = new LambdaOutput(HttpStatus.SC_OK, "");
            }
        }
        catch(Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            e.printStackTrace(System.out);
            // Construimos response de falla
            output = new LambdaOutput(HttpStatus.SC_INTERNAL_SERVER_ERROR, sw.toString());
        }
        return output;
    }*/
}
