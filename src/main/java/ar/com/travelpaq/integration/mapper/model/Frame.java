
package ar.com.travelpaq.integration.mapper.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Frame {

    @JsonProperty("transport_company_id")
    private Integer transport_company_id;

    @JsonProperty("departure_place_id")
    private Integer departure_place_id;

    @JsonProperty("arrival_place_id")
    private Integer arrival_place_id;

    @JsonProperty("direction")
    private int direction;

    @JsonProperty("travel_number")
    private String travel_number;

    @JsonProperty("transport_company")
    private TransportCompany transport_company;

    @JsonProperty("departure_time")
    private String departure_time;

    @JsonProperty("departure_date")
    private String departure_date;

    @JsonProperty("arrival_time")
    private String arrival_time;

    @JsonProperty("arrival_date")
    private String arrival_date;

    @JsonProperty("order_number")
    private int order_number;

    @JsonProperty("departure_place")
    private Place departure_place;

    @JsonProperty("arrival_place")
    private Place arrival_place;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Frame() {
		// Default constructor
	}

	public Frame(int direction, int order_number, String travel_number, TransportCompany transport_company, String departure_time, String departure_date, String arrival_time, String arrival_date, Place departure_place, Place arrival_place) {
		this.direction = direction;
		this.order_number = order_number;
		this.travel_number = travel_number;
		this.transport_company = transport_company;
		this.departure_time = departure_time;
		this.departure_date = departure_date;
		this.arrival_time = arrival_time;
		this.arrival_date = arrival_date;
		this.departure_place = departure_place;
		this.arrival_place = arrival_place;
	}

    @JsonProperty("transport_company_id")
    public Integer getTransportCompanyId() {
        return transport_company_id;
    }

    @JsonProperty("transport_company_id")
    public void setTransportCompanyId(Integer transport_company_id) {
        this.transport_company_id = transport_company_id;
    }

    @JsonProperty("departure_place_id")
    public Integer getDeparturePlaceId() {
        return departure_place_id;
    }

    @JsonProperty("departure_place_id")
    public void setDeparturePlaceId(Integer departure_place_id) {
        this.departure_place_id = departure_place_id;
    }

    @JsonProperty("arrival_place_id")
    public Integer getArrivalPlaceId() {
        return arrival_place_id;
    }

    @JsonProperty("arrival_place_id")
    public void setArrivalPlaceId(Integer arrival_place_id) {
        this.arrival_place_id = arrival_place_id;
    }

    @JsonProperty("direction")
    public int getDirection() {
        return direction;
    }

    @JsonProperty("direction")
    public void setDirection(int direction) {
        this.direction = direction;
    }

    @JsonProperty("travel_number")
    public String getTravelNumber() {
        return travel_number;
    }

    @JsonProperty("travel_number")
    public void setTravelNumber(String travel_number) {
        this.travel_number = travel_number;
    }

    @JsonProperty("arrival_time")
    public String getArrivalTime() {
        return arrival_time;
    }

    @JsonProperty("arrival_time")
    public void setArrivalTime(String arrival_time) {
        this.arrival_time = arrival_time;
    }

    @JsonProperty("arrival_date")
    public String getArrivalDate() {
        return arrival_date;
    }

    @JsonProperty("arrival_date")
    public void setArrivalDate(String arrival_date) {
        this.arrival_date = arrival_date;
    }

    @JsonProperty("order_number")
    public int getOrderNumber() {
        return order_number;
    }

    @JsonProperty("order_number")
    public void setOrderNumber(int order_number) {
        this.order_number = order_number;
    }

    @JsonProperty("departure_time")
    public String getDepartureTime() {
        return departure_time;
    }

    @JsonProperty("departure_time")
    public void setDepartureTime(String departure_time) {
        this.departure_time = departure_time;
    }

    @JsonProperty("departure_date")
    public String getDepartureDate() {
        return departure_date;
    }

    @JsonProperty("departure_date")
    public void setDepartureDate(String departure_date) {
        this.departure_date = departure_date;
    }

    @JsonProperty("departure_place")
    public Place getDeparturePlace() {
        return departure_place;
    }

    @JsonProperty("departure_place")
    public void setDeparturePlace(Place departure_place) {
        this.departure_place = departure_place;
    }

    @JsonProperty("transport_company")
    public TransportCompany getTransportCompany() {
        return transport_company;
    }

    @JsonProperty("transport_company")
    public void setTransportCompany(TransportCompany transport_company) {
        this.transport_company = transport_company;
    }

    @JsonProperty("arrival_place")
    public Place getArrivalPlace() {
        return arrival_place;
    }

    @JsonProperty("arrival_place")
    public void setArrivalPlace(Place arrival_place) {
        this.arrival_place = arrival_place;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
