
package ar.com.travelpaq.integration.mapper.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


public class ChildrenPrice {

    @JsonProperty("age_from")
    private int age_from;

    @JsonProperty("age_to")
    private int age_to;

    @JsonProperty("number_order")
    private int number_order;

    @JsonProperty("max_number_children")
    private int max_number_children;

    @JsonProperty("total_price")
    private TotalPrice total_price;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public ChildrenPrice() {
		// Default constructor
	}

	public ChildrenPrice(int age_from, int age_to, int number_order, int max_number_children, TotalPrice total_price) {
		this.age_from = age_from;
		this.age_to = age_to;
		this.number_order = number_order;
		this.max_number_children = max_number_children;
		this.total_price = total_price;
	}

    @JsonProperty("age_from")
    public int getAgeFrom() {
        return age_from;
    }

    @JsonProperty("age_from")
    public void setAgeFrom(int ageFrom) {
        this.age_from = ageFrom;
    }

    @JsonProperty("age_to")
    public int getAgeTo() {
        return age_to;
    }

    @JsonProperty("age_to")
    public void setAgeTo(int ageTo) {
        this.age_to = ageTo;
    }

    @JsonProperty("number_order")
    public int getNumberOrder() {
        return number_order;
    }

    @JsonProperty("number_order")
    public void setNumberOrder(int number_order) {
        this.number_order = number_order;
    }

    @JsonProperty("max_number_children")
    public int getMaxNumberChildren() {
        return max_number_children;
    }

    @JsonProperty("max_number_children")
    public void setMaxNumberChildren(int maxNumberChildren) {
        this.max_number_children = maxNumberChildren;
    }

    @JsonProperty("total_price")
    public TotalPrice getTotalPrice() {
        return total_price;
    }

    @JsonProperty("total_price")
    public void setTotalPrice(TotalPrice total_price) {
        this.total_price = total_price;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
