
package ar.com.travelpaq.integration.mapper.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Avail {

    @JsonProperty("seats")
    private int seats;

    @JsonProperty("rooms")
    private int rooms;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Avail() {
		// Default constructor
	}

	public Avail(int seats, int rooms) {
		this.seats = seats;
		this.rooms = rooms;
	}

    @JsonProperty("seats")
    public int getSeats() {
        return seats;
    }

    @JsonProperty("seats")
    public void setSeats(int seats) {
        this.seats = seats;
    }

    @JsonProperty("rooms")
    public int getRooms() {
        return rooms;
    }

    @JsonProperty("rooms")
    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
