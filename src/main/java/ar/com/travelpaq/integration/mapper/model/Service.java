
package ar.com.travelpaq.integration.mapper.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Service {

    @JsonProperty("service_kind_id")
    private Integer service_kind_id;

    @JsonProperty("detail")
    private String detail;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Service() {
		// Default constructor
	}

	public Service(String detail) {
		this.detail = detail;
	}

    @JsonProperty("service_kind_id")
    public Integer getServiceKindId() {
        return service_kind_id;
    }

    @JsonProperty("service_kind_id")
    public void setServiceKindId(Integer service_kind_id) {
        this.service_kind_id = service_kind_id;
    }

    @JsonProperty("detail")
    public String getDetail() {
        return detail;
    }

    @JsonProperty("detail")
    public void setDetail(String detail) {
        this.detail = detail;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
