
package ar.com.travelpaq.integration.mapper.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Price {

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("base")
    private int base;

    @JsonProperty("max_number_children")
    private int max_number_children;

    @JsonProperty("total_price")
    private TotalPrice total_price;

    @JsonProperty("children_prices")
    private List<ChildrenPrice> children_prices;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Price() {
		// Default constructor
	}

	public Price(String currency, int base, int max_number_children, TotalPrice total_price, List<ChildrenPrice> children_prices) {
		this.currency = currency;
		this.base = base;
		this.max_number_children = max_number_children;
		this.total_price = total_price;
		this.children_prices = children_prices;
	}

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("base")
    public Integer getBase() {
        return base;
    }

    @JsonProperty("base")
    public void setBase(Integer base) {
        this.base = base;
    }

    @JsonProperty("max_number_children")
    public Integer getMaxNumberChildren() {
        return max_number_children;
    }

    @JsonProperty("max_number_children")
    public void setMaxNumberChildren(Integer max_number_children) {
        this.max_number_children = max_number_children;
    }

    @JsonProperty("total_price")
    public TotalPrice getTotalPrice() {
        return total_price;
    }

    @JsonProperty("total_price")
    public void setTotalPrice(TotalPrice total_price) {
        this.total_price = total_price;
    }

    @JsonProperty("children_prices")
    public List<ChildrenPrice> getChildrenPrices() {
        return children_prices;
    }

    @JsonProperty("children_prices")
    public void setChildrenPrices(List<ChildrenPrice> children_prices) {
        this.children_prices = children_prices;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
