
package ar.com.travelpaq.integration.mapper.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Departure {

    @JsonProperty("place_id")
    private Integer place_id;

    @JsonProperty("date")
    private String date;
    
    @JsonProperty("transport_kind")
    private String transport_kind;

    @JsonProperty("routes")
    private List<Frame> routes = null;

    @JsonProperty("place")
    private Place place;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Departure() {
		// Default constructor
	}

	public Departure(String date, String transport_kind, Place place, List<Frame> routes) {
		this.date = date;
		this.transport_kind = transport_kind;
		this.place = place;
		this.routes = routes;
	}

    @JsonProperty("place_id")
    public Integer getPlaceId() {
        return place_id;
    }

    @JsonProperty("place_id")
    public void setPlaceId(Integer place_id) {
        this.place_id = place_id;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("transport_kind")
    public String getTransportKind() {
        return transport_kind;
    }

    @JsonProperty("transport_kind")
    public void setTransportKind(String transport_kind) {
        this.transport_kind = transport_kind;
    }

    @JsonProperty("routes")
    public List<Frame> getRoutes() {
        return routes;
    }

    @JsonProperty("routes")
    public void setRoutes(List<Frame> route) {
        this.routes = route;
    }

    @JsonProperty("place")
    public Place getPlace() {
        return place;
    }

    @JsonProperty("place")
    public void setPlace(Place place) {
        this.place = place;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
