
package ar.com.travelpaq.integration.mapper.model;

import java.util.List;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Package {

    @JsonProperty("title")
    private String title;

    @JsonProperty("observations")
    private String observations;

    @JsonProperty("itinerary")
    private String itinerary;

    @JsonProperty("expiration_date")
    private String expirationDate;

    @JsonProperty("external_id")
    private String externalId;

    @JsonProperty("services")
    private Set<Service> services;

    @JsonProperty("departure")
    private Departure departure;

    @JsonProperty("places")
    private Set<Place> places;

    @JsonProperty("company")
    private Company company;

    @JsonProperty("accommodations")
    private List<Accommodation> accommodations;

    @JsonProperty("avail")
    private Avail avail;

    @JsonProperty("prices")
    private Set<Price> prices;

}
