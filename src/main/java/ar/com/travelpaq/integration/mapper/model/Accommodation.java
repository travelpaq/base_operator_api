
package ar.com.travelpaq.integration.mapper.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Accommodation {

    @JsonProperty("room_kind_id")
    private Integer room_kind_id;

    @JsonProperty("hotel_id")
    private Integer hotel_id;

    @JsonProperty("regime_id")
    private Integer regime_id;

    @JsonProperty("number_nights")
    private int number_nights;

    @JsonProperty("check_in")
    private String check_in;

    @JsonProperty("check_out")
    private String check_out;

    @JsonProperty("room_kind")
    private String room_kind;

    @JsonProperty("regime")
    private String regime;

    @JsonProperty("hotel")
    private Hotel hotel;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Accommodation() {
		// Default constructor
	}

	public Accommodation(int number_nights, String check_in, String check_out, String room_kind, String regime, Hotel hotel) {
		this.number_nights = number_nights;
		this.check_in = check_in;
		this.check_out = check_out;
		this.room_kind = room_kind;
		this.regime = regime;
		this.hotel = hotel;
	}

    @JsonProperty("room_kind_id")
    public Integer getRoomKindId() {
        return room_kind_id;
    }

    @JsonProperty("room_kind_id")
    public void setRoomKindId(Integer room_kind_id) {
        this.room_kind_id = room_kind_id;
    }

    @JsonProperty("hotel_id")
    public Integer getHotelId() {
        return hotel_id;
    }

    @JsonProperty("hotel_id")
    public void setHotelId(Integer hotel_id) {
        this.hotel_id = hotel_id;
    }

    @JsonProperty("regime_id")
    public Integer getRegimeId() {
        return regime_id;
    }

    @JsonProperty("regime_id")
    public void setRegimeId(Integer regime_id) {
        this.regime_id = regime_id;
    }

    @JsonProperty("number_nights")
    public int getNumberNights() {
        return number_nights;
    }

    @JsonProperty("number_nights")
    public void setNumberNights(int numberNights) {
        this.number_nights = numberNights;
    }

    @JsonProperty("check_in")
    public String getCheckIn() {
        return check_in;
    }

    @JsonProperty("check_in")
    public void setCheckIn(String checkIn) {
        this.check_in = checkIn;
    }

    @JsonProperty("check_out")
    public String getCheckOut() {
        return check_out;
    }

    @JsonProperty("check_out")
    public void setCheckOut(String checkOut) {
        this.check_out = checkOut;
    }

    @JsonProperty("room_kind")
    public String getRoomKind() {
        return room_kind;
    }

    @JsonProperty("room_kind")
    public void setRoomKind(String roomKind) {
        this.room_kind = roomKind;
    }

    @JsonProperty("regime")
    public String getRegime() {
        return regime;
    }

    @JsonProperty("regime")
    public void setRegime(String regime) {
        this.regime = regime;
    }

    @JsonProperty("hotel")
    public Hotel getHotel() {
        return hotel;
    }

    @JsonProperty("hotel")
    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
