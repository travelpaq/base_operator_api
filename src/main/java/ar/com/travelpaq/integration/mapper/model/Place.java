
package ar.com.travelpaq.integration.mapper.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Place {

    @JsonProperty("place_id")
    private Integer place_id;

    @JsonProperty("iata")
    private String iata;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Place() {
		// Deafult constructor
	}

	public Place(String iata) {
		this.iata = iata;
	}

    @JsonProperty("place_id")
    public Integer getPlaceId() {
        return place_id;
    }

    @JsonProperty("place_id")
    public void setPlaceId(Integer place_id) {
        this.place_id = place_id;
    }

    @JsonProperty("iata")
    public String getIata() {
        return iata;
    }

    @JsonProperty("iata")
    public void setIata(String iata) {
        this.iata = iata;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
