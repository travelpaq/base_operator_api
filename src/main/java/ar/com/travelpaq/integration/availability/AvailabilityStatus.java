package ar.com.travelpaq.integration.availability;

public class AvailabilityStatus {
    public static String AVAILABLE = "AVAILABLE";
    public static String NOT_AVAILABLE = "NOT_AVAILABLE";
}
