package ar.com.travelpaq.integration.availability.model;

import ar.com.travelpaq.integration.commons.model.domain.Package;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PackageStatus {

    @JsonProperty("status")
    private String status;

    @JsonProperty("message")
    private String message;

    @JsonProperty("Package")
    private Package _package;

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("Package")
    public Package get_package() {
        return _package;
    }

    @JsonProperty("Package")
    public void set_package(Package _package) {
        this._package = _package;
    }
}
