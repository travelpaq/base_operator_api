package ar.com.travelpaq.integration.availability;

import ar.com.travelpaq.integration.availability.model.PackageStatus;
import ar.com.travelpaq.integration.commons.configuration.ExecutionMode;
import ar.com.travelpaq.integration.commons.connection.ConnectionData;
import ar.com.travelpaq.integration.commons.connection.Integration;
import ar.com.travelpaq.integration.commons.model.Persistence;
import ar.com.travelpaq.integration.commons.model.domain.RoomPrice;
import ar.com.travelpaq.integration.commons.model.domain.Package;
import ar.com.travelpaq.integration.commons.utils.MessagesList;

import java.io.IOException;
import java.util.List;

public abstract class BaseAvailability {

    protected String checkMessage;
    protected Package _package;
    protected ConnectionData connectionData;
    private ExecutionMode executionMode;

    public BaseAvailability(Integer companyId, Package _package, ExecutionMode executionMode) throws IOException {
        this.connectionData = Integration.getConnectionData(companyId, executionMode);
        this._package = _package;
        this.executionMode = executionMode;
    }

    public PackageStatus check() throws Exception {

        PackageStatus packageStatus = new PackageStatus();

        int amount_of_rooms = _package.getPrice().getRoomsPrice().size();
        int amount_of_seats = 0;
        for (RoomPrice roomPrice:_package.getPrice().getRoomsPrice())
            amount_of_seats += roomPrice.getAdultPrice().getAdult() + roomPrice.getChildren().size();

        if(amount_of_rooms <= 0 || amount_of_seats <= 0 || amount_of_rooms > amount_of_seats){
            packageStatus.setStatus("NOT_AVAILABLE");
            packageStatus.setMessage(MessagesList.AVAILABILITY_BAD_PARAMETERS);
            packageStatus.set_package(_package);
        }


        Object uniqueId = buildFareId(_package.getExternalId(), _package.getPrice().getRoomsPrice());

        Boolean checkResult = supplierCheckAvail(uniqueId, amount_of_seats, amount_of_rooms);

        Persistence.updateAvail(_package, executionMode);

        packageStatus.setStatus((checkResult ? "AVAILABLE":"NOT_AVAILABLE"));
        packageStatus.setMessage(checkMessage);
        packageStatus.set_package(_package);

        return packageStatus;
    }

    public abstract Boolean supplierCheckAvail(Object uniqueId, int amount_of_seats, int amount_of_rooms);

    public abstract Object buildFareId(String externalId, List<RoomPrice> rooms);
}