package ar.com.travelpaq.integration.extractor.artifact;

public class ExtractorInput {
    private int operatorId;
    private int monthToExtract;
    private int initialMonth;
    private int initialYear;
    private String destinationToDump;
    private boolean lateSendMessages;

    public ExtractorInput(){
        super();
        destinationToDump = "";
    }

    public int getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }

    public int getMonthToExtract() {
        return monthToExtract;
    }

    public void setMonthToExtract(int monthToExtract) {
        this.monthToExtract = monthToExtract;
    }

    public int getInitialMonth() {
        return initialMonth;
    }

    public void setInitialMonth(int initialMonth) {
        this.initialMonth = initialMonth;
    }

    public int getInitialYear() {
        return initialYear;
    }

    public void setInitialYear(int initialYear) {
        this.initialYear = initialYear;
    }

    public String getDestinationToDump() {
        return destinationToDump;
    }

    public void setDestinationToDump(String destinationToDump) {
        this.destinationToDump = destinationToDump;
    }

    public boolean getLateSendMessages() {
        return lateSendMessages;
    }

    public void setLateSendMessages(boolean lateSendMessages) {
        this.lateSendMessages = lateSendMessages;
    }
}
