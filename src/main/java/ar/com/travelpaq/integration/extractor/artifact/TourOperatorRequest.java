package ar.com.travelpaq.integration.extractor.artifact;

import ar.com.travelpaq.integration.commons.configuration.ExecutionMode;
import ar.com.travelpaq.integration.commons.connection.ConnectionData;
import ar.com.travelpaq.integration.commons.utils.QueueMessage;
import ar.com.travelpaq.integration.commons.utils.QueueName;
import ar.com.travelpaq.integration.commons.utils.QueueService;
import ar.com.travelpaq.integration.mapper.model.Package;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;


import java.io.IOException;
import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;

public class TourOperatorRequest extends RecursiveAction {
	
	private Boolean processed;
	
	private long ttl;

	private Method method;

    private TreeNode container;

    private Boolean isIndependent;

	private Boolean isPrintable;

	private Boolean haveToKeepBody;

	private String body;

	private Boolean complete;

	private String infoException;

	private ConnectionData connectionData;

	private ExecutionMode executionMode;

	private QueueName queueName;

	private String extraction;

	//private String region;

	public String getBody() { return body; }
	public void setBody(String body) { this.body = body; }
    public Boolean getIndependent() { return isIndependent; }
    public void setIndependent(Boolean independent) { this.isIndependent = independent; }
    public Boolean getPrintable() { return isPrintable; }
    public void setPrintable(Boolean printable) {this.isPrintable = printable;}
    public TreeNode getContainer() { return container; }
    public void setContainer(TreeNode container) { this.container = container; }
    public long getTtl() {
		return ttl;
	}
    public void setTtl(long ttl) {
		this.ttl = ttl;
	}
    public Boolean getProcessed() {
		return processed;
	}
    public void setProcessed(Boolean processed) {
		this.processed = processed;
	}
    public Method getMethod() { return method;}
    public void setMethod(Method method) { this.method = method; }

	public Boolean getComplete() {
		return complete;
	}

	public void setComplete(Boolean complete) {
		this.complete = complete;
	}

	public String getInfoException() {
		return infoException;
	}

	public void setInfoException(String infoException) {
		this.infoException = infoException;
	}

	public TourOperatorRequest(Boolean independent, Boolean printable, Boolean haveToKeepBody, Method method, ConnectionData connectionData, ExecutionMode executionMode, String integration, String extraction) {
		this.isIndependent = independent;
		this.isPrintable = printable;
		this.method = method;
		this.ttl = 0;
		this.processed = false;
		this.haveToKeepBody = haveToKeepBody;
		this.complete = false;
		this.connectionData = connectionData;
		this.executionMode = executionMode;
		this.extraction = extraction;
		//this.region = "us-east-1";

		if (executionMode == ExecutionMode.PROD){

			switch (integration){
				case "softur": 		{this.queueName = QueueName.STANDARDIZATION_SOFTUR;break;}
				case "ola":    		{this.queueName = QueueName.STANDARDIZATION_OLA;break;}
				case "amichi": 		{this.queueName = QueueName.STANDARDIZATION_AMICHI;break;}
				case "delfos": 		{this.queueName = QueueName.STANDARDIZATION_DELFOS;break;}
				case "julia": 		{this.queueName = QueueName.STANDARDIZATION_JULIA;break;}
				case "pythagoras": 	{this.queueName = QueueName.STANDARDIZATION_PYTHAGORAS;break;}
				case "redevt": 		{this.queueName = QueueName.STANDARDIZATION_REDEVT;break;}
				case "aero": 		{this.queueName = QueueName.STANDARDIZATION_AERO;break;}
				default: this.queueName = QueueName.STANDARDIZATION;
			}
		}
		else
			this.queueName = QueueName.valueOf("standardization_dev");
	}

	@Override
	protected void compute() {
		this.processed = true;
		try {
			// Get execution time
			long start = System.currentTimeMillis();
			method.call();
			String response = method.getString();
			ttl = System.currentTimeMillis() - start;

			//-------------------
			if(response != null){
				complete = true;

			}

			body = response;

			if(isPrintable){
				if(method.getPackagesAmount() > 0){
					if(!isIndependent) {
						HashMap<String, String> HashResponse = recursivePrint(new HashMap<>());
						response = new ObjectMapper().writeValueAsString(HashResponse);
					}
					List<List<Package>> packageMessages = splitPackagesResponse(response);
					List<ParallelSender> senders = new ArrayList<>(packageMessages.size());
					String hash = LocalDate.now().toString() + connectionData.getCompanyId() + container.getPlace() +  container.getDateFrom() + container.getDateTo();
					for (int i = 0;i < packageMessages.size();i++){
						senders.add(new ParallelSender(packageMessages.get(i), connectionData.getCompanyId(), queueName.getQueueName(), container.getPlace(), container.getDateFrom(), container.getDateTo(), hash, String.valueOf(i), LocalDate.now().toString(), extraction));
					}

					ForkJoinTask.invokeAll(senders);

					method.destroyResponse();
				}
			}

			if(!haveToKeepBody) {
				body = null;
			}

		} catch (Exception e) {
			e.printStackTrace(System.out);
			infoException = e.getMessage();
		}
	}

	private HashMap<String, String> recursivePrint(HashMap<String, String> message) {
		message.put(method.getClass().toString(), body);
		if(container.getParent() != null)
			return container.getParent().getRequest().recursivePrint(message);
		else
			return message;
	}

	private  List<List<Package>> splitPackagesResponse(String packageResponse){

		List<List<Package>> packageMessages = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		JavaType listType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, Package.class);
		List<Package> packageList;
		try {
			packageList = mapper.readValue(packageResponse, listType);
		} catch (IOException e) {
			e.printStackTrace(System.out);
			return new ArrayList<>();
		}

		Boolean haveMore;
		int index = 0;
		int sizeOfBatch = 50;
		do{
			haveMore = false;
			List<Package> subListPackage = new ArrayList<>();
			Integer amountOfPackage = 0;
			for(int i = index;i < packageList.size();i++){
				subListPackage.add(packageList.get(i));
				index++;
				amountOfPackage++;
				if (amountOfPackage >= sizeOfBatch)
					break;
			}

			if (index < packageList.size())
				haveMore = true;

			packageMessages.add(subListPackage);

		} while (haveMore);

		return packageMessages;

	}

	public ExecutionMode getExecutionMode() {
		return executionMode;
	}

	public void setExecutionMode(ExecutionMode executionMode) {
		this.executionMode = executionMode;
	}
}