package ar.com.travelpaq.integration.extractor;

import ar.com.travelpaq.integration.commons.configuration.ExecutionMode;
import ar.com.travelpaq.integration.commons.connection.ConnectionData;
import ar.com.travelpaq.integration.commons.connection.Integration;
import ar.com.travelpaq.integration.commons.model.DataSource;
import ar.com.travelpaq.integration.extractor.artifact.TourOperatorRequest;
import ar.com.travelpaq.integration.extractor.artifact.TreeNode;
import ar.com.travelpaq.integration.extractor.model.ExtractorMetrics;
import ar.com.travelpaq.integration.extractor.model.WrongRequests;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;


import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ForkJoinTask;

/**
 * Created by mauro on 12/16/17.
 */
public abstract class BaseExtractor {
    private float ttlAverage;
    private float ttlStandardDeviation;
    private ArrayList<TreeNode> nodesToExecute;
    private int globalAmountRequest;
    private Boolean isGoingBack;
    private TreeNode tree;
    private Boolean optimusAmountRequest;
    private ArrayList<TreeNode> wrongsNodes;
    private int amountRequestsProcessed;
    private long totalTime;
    private Boolean secondChance;
    private Boolean lateSendMessages;
    protected int company_id;
    protected DataSource db_source;
    protected List<String> fromMonths;
    protected List<String> toMonths;
    protected ExtractorMetrics extractorMetrics;
    protected int monthToExtract;
    protected int initialMonth;
    protected int initialYear;
    protected ConnectionData connectionData;
    protected String destination_to_dump;
    private ExecutionMode excecutionMode;
    protected String db_endpoint;
    protected String db_pwd;
    protected String db_username;
    protected String dev_db_endpoint;
    protected Integer parallelExecutionLimit;


    public BaseExtractor(int company_id, int monthToExtract, int initialMonth, int initialYear, ExecutionMode excecutionMode) throws Exception {
        this(company_id, monthToExtract, initialMonth, initialYear, "",false, excecutionMode);
    }

    public BaseExtractor(int company_id, int monthToExtract, int initialMonth, int initialYear, String destination_to_dump, ExecutionMode excecutionMode) throws Exception {
        this(company_id, monthToExtract, initialMonth, initialYear, destination_to_dump,false, excecutionMode);
    }

    public BaseExtractor(int company_id, int monthToExtract, int initialMonth, int initialYear, Boolean lateSendMessages, ExecutionMode excecutionMode) throws Exception {
        this(company_id, monthToExtract, initialMonth, initialYear, "",lateSendMessages, excecutionMode);
    }

    public BaseExtractor(int company_id, int monthToExtract, int initialMonth, int initialYear, String destination_to_dump, Boolean lateSendMessages, ExecutionMode excecutionMode) throws Exception {

        // Initialization of variables
        this.parallelExecutionLimit = 100;
        this.optimusAmountRequest = false;
        this.isGoingBack = false;
        this.nodesToExecute = new ArrayList<TreeNode>();
        this.globalAmountRequest = 1;
        this.isGoingBack = false;
        this.ttlAverage = 0;
        this.ttlStandardDeviation = 0;
        this.secondChance = false;
        this.extractorMetrics = new ExtractorMetrics();
        this.db_source = new DataSource();
        this.extractorMetrics.setCompanyId(company_id);
        this.company_id = company_id;
        this.monthToExtract = monthToExtract;
        this.initialMonth = initialMonth;
        this.initialYear = initialYear;
        this.connectionData = Integration.getConnectionData(company_id, excecutionMode);
        this.connectionData.setCompanyId(company_id);
        this.destination_to_dump = destination_to_dump;
        this.lateSendMessages = lateSendMessages;
        this.excecutionMode = excecutionMode;
        this.db_endpoint = System.getenv("DB_ENDPOINT").replace("packages?useSSL=false&socketFactory=com.google.cloud.sql.mysql.SocketFactory&cloudSqlInstance=packages-267220:us-central1:packages","logs?useSSL=false&socketFactory=com.google.cloud.sql.mysql.SocketFactory&cloudSqlInstance=packages-267220:us-central1:packages");
        this.db_pwd = System.getenv("DB_PWD");
        this.db_username = System.getenv("DB_USERNAME");
        this.dev_db_endpoint = db_endpoint.replace("://packages.","://packages-dev.");

        //----------------------------

        //Get the initial Tree of request and the requestStack
        long start = System.currentTimeMillis();
        tree = initTree();
        long elapsedTimeMillis = System.currentTimeMillis() - start;
        totalTime = (int)(elapsedTimeMillis/(1000F));
        //----------------------------------------------------
    }

    public ExtractorMetrics extract() {

        long start = System.currentTimeMillis();

        ArrayList<TreeNode> nodeWideLevel;
        int amountRequest = globalAmountRequest;
        int index = 0;
        nodeWideLevel = tree.getChildren();

        wrongsNodes = new ArrayList<>();

        while(nodeWideLevel.size() > 0) {
            for(int i = 0;i < amountRequest;i++) {
                if(index < nodeWideLevel.size()) {
                    if(!nodeWideLevel.get(index).getRequest().getComplete())
                        if(destination_to_dump.equals("")) {
                            nodesToExecute.add(nodeWideLevel.get(index));
                        } else {
                            if (destination_to_dump.equals(nodeWideLevel.get(index).getPlace())){
                                nodesToExecute.add(nodeWideLevel.get(index));
                            } else {
                                i--;
                                nodeWideLevel.get(index).getRequest().setProcessed(true);
                                nodeWideLevel.get(index).getRequest().setComplete(true);
                            }
                        }
                    index++;
                } else {
                    if(nodeWideLevel.size() > 0){
                        if (nodesToExecute.size() > 0){
                            parallelExecutionRequest();
                            amountRequest = globalAmountRequest;
                        }
                        ArrayList<TreeNode> childrenNodeWideLevel = new ArrayList<>();
                        for(TreeNode treeNode:nodeWideLevel){
                            if(treeNode.getRequest().getComplete())
                                childrenNodeWideLevel.addAll(treeNode.getChildren());
                            else
                                wrongsNodes.add(treeNode);
                        }
                        nodeWideLevel = childrenNodeWideLevel;
                        i--;
                        index = 0;
                    }
                }
            }
            if (nodesToExecute.size() > 0){
                parallelExecutionRequest();
                amountRequest = globalAmountRequest;
            }
        }


        if(!secondChance && wrongsNodes.size() > 0){
            secondChance = true;
            extract();
        }
        else {

            if (lateSendMessages)
                sendLateMessages();

            long elapsedTimeMillis = System.currentTimeMillis() - start;
            totalTime += (int)(elapsedTimeMillis/(1000F));

            for (int i = 0;i < wrongsNodes.size();i++){
                extractorMetrics.getWrongRequests().add(new WrongRequests(
                        extractorMetrics,
                        wrongsNodes.get(i).getRequest().getMethod().getClass().toString(),
                        wrongsNodes.get(i).getRequest().getInfoException(),
                        (int) wrongsNodes.get(i).getRequest().getTtl(),
                        wrongsNodes.get(i).getDateFrom(),
                        wrongsNodes.get(i).getDateTo(),
                        wrongsNodes.get(i).getPlace()
                ));
            }

            extractorMetrics.setAmountRequestsProcessed(amountRequestsProcessed);
            extractorMetrics.setAmountRequestsWithErrors(wrongsNodes.size());
            extractorMetrics.setTotalTime((int)totalTime);
            extractorMetrics.setOptimusRequestAmount(globalAmountRequest);
            extractorMetrics.setTtlAverage((int)ttlAverage);
            extractorMetrics.setTtlStandardDeviation((int)ttlStandardDeviation);
            persist_metrics();
        }


        return extractorMetrics;
    }

    public void parallelExecutionRequest() {

        //Build the requests list
        ArrayList<TourOperatorRequest> requests = new ArrayList<TourOperatorRequest>();
        for(TreeNode node:nodesToExecute) {
            requests.add(node.getRequest());
        }
        //-----------------------

        //Execute the request in parallel and wait each one finish
        amountRequestsProcessed += requests.size();
        ForkJoinTask.invokeAll(requests);
        //--------------------------------------------------------

        //Register info
        for(int i = 0;i < nodesToExecute.size();i++){
            if(nodesToExecute.get(i).getRequest().getComplete()){
                extractorMetrics.setPackagesAmount(extractorMetrics.getPackagesAmount() + nodesToExecute.get(i).getRequest().getMethod().getPackagesAmount());
            }
        }
        //--------------

        //Set to empty list the list of requests to execute
        nodesToExecute = new ArrayList<TreeNode>();
        //-------------------------------------------------

        //Set the amount of request to trigger in parallel
        if(!optimusAmountRequest) {
            setGlobalAmountRequest(requests);
        }
        //------------------------------------------------


    }

    public void setGlobalAmountRequest(ArrayList<TourOperatorRequest> requests) {

        //Set average and standard deviation per package
        ArrayList<Long> ttlList = new ArrayList<Long>();
        for(TourOperatorRequest request:requests) {
            if(request.getMethod().getPackagesAmount() > 0)
                ttlList.add(request.getTtl() / request.getMethod().getPackagesAmount());
        }
        //----------------------------------

        float localTtlAverage = average(ttlList);
        float localTtlStandardDeviation = standard_deviation(ttlList);

        if(ttlAverage == 0 || (localTtlAverage <= (ttlAverage + ttlStandardDeviation + ((ttlAverage + ttlStandardDeviation) / globalAmountRequest)))) {
            if(isGoingBack){
                optimusAmountRequest = true;
            } else {
                globalAmountRequest *= 2;

                if(ttlAverage == 0)
                    ttlAverage = localTtlAverage;
                else
                if(ttlAverage < localTtlAverage)
                    ttlAverage = (ttlAverage + localTtlAverage) / 2;

                if(ttlStandardDeviation == 0)
                    ttlStandardDeviation = localTtlStandardDeviation;
                else
                if(ttlStandardDeviation < localTtlStandardDeviation)
                    ttlStandardDeviation = (ttlStandardDeviation + localTtlStandardDeviation) / 2;
            }
        } else {
            isGoingBack = true;
            if(globalAmountRequest > 1)
                globalAmountRequest--;
            else
                optimusAmountRequest = true;

        }

        if(globalAmountRequest > parallelExecutionLimit){
            globalAmountRequest = parallelExecutionLimit;
            optimusAmountRequest = true;
        }
    }

    public float average(ArrayList<Long> numbers) {
        float average = 0;
        if(numbers.size() > 0){
            int sum = 0;
            for(Long number:numbers) {
                sum+= number;
            }
            average = sum / numbers.size();
        }
        return average;
    }

    public float standard_deviation(ArrayList<Long> numbers) {
        float average = average(numbers);
        float standard_deviation = 0;
        if(numbers.size() > 0){
            for (int i = 0; i < numbers.size(); i++) {
                float range;
                range = (float)Math.pow(numbers.get(i) - average, 2);
                standard_deviation = standard_deviation + range;
            }
            standard_deviation = standard_deviation / numbers.size();
        }
        return (float) Math.sqrt(standard_deviation);
    }

    public void persist_metrics (){

        try {
            //Create configuration for the conection with de database
            Configuration configuration = new Configuration();
            configuration.addAnnotatedClass (ExtractorMetrics.class);
            configuration.addAnnotatedClass (WrongRequests.class);

            configuration.setProperty("hibernate.bytecode.use_reflection_optimizer", "true");
            configuration.setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");

            if (excecutionMode == ExecutionMode.PROD)
                configuration.setProperty("hibernate.connection.url", db_endpoint);
            else
                configuration.setProperty("hiberna te.connection.url", dev_db_endpoint);

            configuration.setProperty("hibernate.connection.username", db_username);
            configuration.setProperty("hibernate.connection.password", db_pwd);

            configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
            configuration.setProperty("hibernate.search.autoregister_listeners", "false");
            configuration.setProperty("hibernate.cache.use_second_level_cache", "false");
            configuration.setProperty("hibernate.jdbc.batch_size", "50");
            configuration.setProperty("hibernate.order_inserts", "true");
            configuration.setProperty("hibernate.jdbc.batch_versioned_data", "true");
            configuration.setProperty("hibernate.validator.apply_to_ddl", "false");


            Session session = configuration.buildSessionFactory().openSession();
            session.beginTransaction();
            session.persist(extractorMetrics);

            session.flush();
            session.clear();
            session.getTransaction().commit();

            session.close();

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    public void setMonthsToDump(int monthToExtract, int initialMonth, int initialYear) throws JAXBException, TransformerException {
        fromMonths = new ArrayList<>();
        toMonths = new ArrayList<>();
        String zero;
        String day;
        int month = 0;
        int year = 0;
        for(int i = 0;i < monthToExtract;i++) {
            month = ((initialMonth - 1 + i) % 12) + 1;
            if(month < initialMonth || (month == initialMonth && i > (initialMonth - 1)))
                year = initialYear + 1;
            else
                year = initialYear;

            if(month < 10)
                zero = "0";
            else
                zero = "";

            if(month == LocalDate.now().getMonth().getValue()){
                if(LocalDate.now().getDayOfMonth() < 10){
                    day = "0" + LocalDate.now().getDayOfMonth();
                } else {
                    day = ((Integer)LocalDate.now().getDayOfMonth()).toString();
                }
            } else {
                day = "01";
            }


            fromMonths.add(year + "-" + zero + month + "-" + day);
            toMonths.add(year + "-" + zero + month + "-" + LocalDate.of(year, month, 1).lengthOfMonth());
        }

    }

    public List<String> getHistoricDestinies(){
        //Active Cities in TravelPAQ
        ArrayList cityCodes = new ArrayList();
        db_source.setConnection(db_endpoint,db_username,db_pwd);
        ResultSet cities;
        try {
            cities = db_source.query(   "SELECT link as iata FROM companies_places LEFT JOIN places ON place_id = places.id WHERE companies_places.company_id = " + company_id + " AND NOT ISNULL(companies_places.place_id) AND companies_places.disabled = 0");
            while(cities.next()){
                cityCodes.add(cities.getString(1));
            }
            return cityCodes;
        } catch (Exception e) {
            e.printStackTrace(System.out);
            extractorMetrics.setGlobalException(e.getMessage());
            return new ArrayList<>();
        }
    }

    public abstract TreeNode initTree();

    public abstract void sendLateMessages();

    public Boolean getLateSendMessages() {
        return lateSendMessages;
    }

    public void setLateSendMessages(Boolean lateSendMessages) {
        this.lateSendMessages = lateSendMessages;
    }

    public TreeNode getTree() {
        return tree;
    }

    public void setTree(TreeNode tree) {
        this.tree = tree;
    }

    public ExecutionMode getExcecutionMode() {
        return excecutionMode;
    }

    public void setExcecutionMode(ExecutionMode excecutionMode) {
        this.excecutionMode = excecutionMode;
    }
}
