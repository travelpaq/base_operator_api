package ar.com.travelpaq.integration.extractor.artifact;

import jdk.internal.dynalink.linker.LinkerServices;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import java.text.ParseException;
import java.util.List;

public interface Method {

    public Object call() throws DatatypeConfigurationException, ParseException, JAXBException, TransformerException;
    public int getPackagesAmount();
    public String getString();
    public void destroyResponse();

}
