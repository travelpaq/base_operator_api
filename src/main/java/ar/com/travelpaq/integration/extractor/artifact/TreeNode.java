package ar.com.travelpaq.integration.extractor.artifact;

import lombok.Data;
import java.util.ArrayList;

@Data
public class TreeNode {
	private TourOperatorRequest request;
	private ArrayList<TreeNode> children;
	private String dateFrom;
	private String dateTo;
	private String place;
	private Integer batchSize;
	private Integer batchNumber;
	private TreeNode parent;

	public TreeNode(TourOperatorRequest request, ArrayList<TreeNode> children, TreeNode parent, String dateFrom, String dateTo, String place, Integer batchSize, Integer batchNumber) {
		this.request = request;
		this.children = children;
		this.parent = parent;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.place = place;
		this.batchSize = batchSize;
		this.batchNumber = batchNumber;
	}
}