package ar.com.travelpaq.integration.extractor.artifact;

import java.util.List;
import java.util.concurrent.RecursiveAction;

import ar.com.travelpaq.integration.commons.utils.ObjectUtils;
import ar.com.travelpaq.integration.commons.utils.QueueMessage;
import ar.com.travelpaq.integration.commons.utils.QueueName;
import ar.com.travelpaq.integration.commons.utils.QueueService;
import ar.com.travelpaq.integration.mapper.model.Package;

public class ParallelSender extends RecursiveAction {

    private List<Package> packageList;
    private QueueName queueName;
    private Integer operatorId;
    private String destination;
    private String dateFrom;
    private String dateTo;
    private String hash;
    private String group;
    private String date;
    private String extraction;

    public ParallelSender(List<Package> packageList, Integer operatorId, String queueName, String destination, String dateFrom, String dateTo, String hash, String group, String date, String extraction) {
        this.packageList = packageList;
        this.operatorId = operatorId;
        this.queueName = QueueName.getEnum(queueName);
        this.destination = destination;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.hash = hash;
        this.group = group;
        this.date = date;
        this.extraction = extraction;
    }

    @Override
    protected void compute() {
        QueueService queue = new QueueService(this.queueName.getQueueRegion());
        QueueMessage message = new QueueMessage(ObjectUtils.convertValue(packageList), operatorId.toString(), queueName.getQueueName(), queueName.getQueueRegion(), destination, dateFrom, dateTo, hash, group, date, extraction);
        queue.sendMessage(queue.getQueueURL(queueName.getQueueName()), message, 0);
    }
}
