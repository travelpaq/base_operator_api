package ar.com.travelpaq.integration.commons.model.domain;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RoomPrice {

    @JsonProperty("adult_price")
    private AdultPrice adultPrice;

    @JsonProperty("children_prices")
    private List<ChildrenPrice> childrenPrices;

    @JsonProperty("total_price")
    private TotalPrice total_price;

    @JsonProperty("final_price")
    private float finalPrice;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public RoomPrice() {
        adultPrice = new AdultPrice();
        childrenPrices = new ArrayList<>();
        total_price = new TotalPrice();
    }

    public RoomPrice(AdultPrice adultPrice, List<ChildrenPrice> childrenPrices, TotalPrice total_price, float finalPrice) {
        this.adultPrice = adultPrice;
        this.childrenPrices = childrenPrices;
        this.total_price = total_price;
        this.finalPrice = finalPrice;
    }

    @JsonProperty("adult_price")
    public AdultPrice getAdultPrice() {
        return adultPrice;
    }

    @JsonProperty("adult_price")
    public void setAdultPrice(AdultPrice adultPrice) {
        this.adultPrice = adultPrice;
    }

    @JsonProperty("children_prices")
    public List<ChildrenPrice> getChildren() {
        return childrenPrices;
    }

    @JsonProperty("children_prices")
    public void setChildren(List<ChildrenPrice> childrenPrices) {
        this.childrenPrices = childrenPrices;
    }

    @JsonProperty("total_price")
    public TotalPrice getTotal_price() {
        return total_price;
    }

    @JsonProperty("total_price")
    public void setTotal_price(TotalPrice total_price) {
        this.total_price = total_price;
    }

    @JsonProperty("final_price")
    public float getFinalPrice() {
        return finalPrice;
    }

    @JsonProperty("final_price")
    public void setFinalPrice(float finalPrice) {
        this.finalPrice = finalPrice;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }
}
