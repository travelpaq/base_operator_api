package ar.com.travelpaq.integration.commons.connection;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Pagination {

    @JsonProperty("total")
    private Integer total;

    @JsonProperty("per_page")
    private Integer per_page;

    @JsonProperty("current_page")
    private Integer current_page;

    @JsonProperty("last_page")
    private Integer last_page;

    public Pagination(Integer total, Integer per_page, Integer current_page, Integer last_page) {
        this.total = total;
        this.per_page = per_page;
        this.current_page = current_page;
        this.last_page = last_page;
    }

    public Pagination() {
        this.total = 0;
        this.per_page = 0;
        this.current_page = 0;
        this.last_page = 0;
    }



    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getPer_page() {
        return per_page;
    }

    public void setPer_page(Integer per_page) {
        this.per_page = per_page;
    }

    public Integer getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(Integer current_page) {
        this.current_page = current_page;
    }

    public Integer getLast_page() {
        return last_page;
    }

    public void setLast_page(Integer last_page) {
        this.last_page = last_page;
    }
}
