package ar.com.travelpaq.integration.commons.utils;

import com.amazon.sqs.javamessaging.AmazonSQSExtendedClient;
import com.amazon.sqs.javamessaging.ExtendedClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// AWS SDK
// AWS Models

public class QueueService 
{
	private AmazonSQS sqsExtendedClient;

	/**
	 *  Constructor del servicio de colas
     *  @param regionName nombre de la region de AWS
	 */
	public QueueService(String regionName) {
		Regions region = Regions.fromName(regionName);

        AWSCredentialsProvider credentialsProvider = new DefaultAWSCredentialsProviderChain();

	    // Creamos el cliente comun para SQS
        AmazonSQS sqsClient = AmazonSQSClient
                .builder()
                .withCredentials(credentialsProvider)
                .withRegion(region)
                .build();

		// Crear el cliente para S3
		AmazonS3 s3client = AmazonS3Client
				.builder()
				.withCredentials(credentialsProvider)
				.withRegion(region)
				.build();

        // Configuramos el cliente extendido con el soporte para mensajes > 256KB habilitado
        ExtendedClientConfiguration extendedClientConfig = new ExtendedClientConfiguration()
                .withLargePayloadSupportEnabled(s3client, "travelpaq-sqs-test-bucket")
                .withAlwaysThroughS3(false);
        sqsExtendedClient = new AmazonSQSExtendedClient(sqsClient, extendedClientConfig);
	}
	
	/**
	 * Devuelve una lista de las URLs de las colas existenes
	 * @return  lista con las URLs.
	 */
	public List<String> getQueuesURLs() {
	    return sqsExtendedClient.listQueues().getQueueUrls();
	}
	
	/**
	 * Devuelve una lista de las URLs de las colas cuyos nombres comienzan con un prefijo especificado
	 * @param  	prefix  el prefijo para realizar la búsqueda
	 * @return 	        lista con las URL que coinciden con el prefijo
	 */
	public List<String> getQueuesURLs(String prefix) {
		return sqsExtendedClient.listQueues(new ListQueuesRequest(prefix)).getQueueUrls();
	}
	
	/**
	 * Devuelve la URL de una cola
	 * @param queueName el nombre de la cola para la cual retornar la URL
     * @return          la URL de la cola especificada
	 */
	public String getQueueURL(String queueName) {
	    return sqsExtendedClient.getQueueUrl(queueName).getQueueUrl();
	}

	/**
	 *  Elimna una cola
     *  @param queueUrl la URL de la cola a eliminar
	 */
	public void deleteQueueByURL(String queueUrl) {
		DeleteQueueRequest delete_request = new DeleteQueueRequest(queueUrl);
        sqsExtendedClient.deleteQueue(delete_request);
	}

	/**
	 *  Crea una nueva cola
     *  @param queueName nombre de la cola a crear
     *  @return resultado de creacion de la cola
	 */
	public String createQueue(String queueName) {
		CreateQueueRequest create_request = new CreateQueueRequest(queueName);
		CreateQueueResult result = sqsExtendedClient.createQueue(create_request);
		return result.getQueueUrl();
	}
	
	/**
	 * Enviar un mensaje a una cola
     * @param queueURL la URL de la cola a la cual se envia el mensaje
     * @param message el mensaje a enviar como String que representa un JSON
     * @param delaySeconds tiempo de espera antes de enviar el mensaje
	 */
	public HashMap<String, String> sendMessage(String queueURL, String message, int delaySeconds) {
		SendMessageRequest send_msg_request = new SendMessageRequest()
		        .withQueueUrl(queueURL)
		        .withMessageBody(message)
		        .withDelaySeconds(delaySeconds);
		SendMessageResult result = sqsExtendedClient.sendMessage(send_msg_request);
		
		// Convertimos el resultado a un hashmap
        HashMap<String, String> resultHash = new HashMap<String, String>();
		resultHash.put("MD5OfMessageBody", result.getMD5OfMessageBody());
		resultHash.put("MessageId", result.getMessageId());
		return resultHash;
	}

	/**
	 * Enviar un mensaje a una cola
	 * @param queueURL la URL de la cola a la cual se envia el mensaje
	 * @param message el mensaje a enviar como Class QueueMessage
	 * @param delaySeconds tiempo de espera antes de enviar el mensaje
	 */
	public HashMap<String, String> sendMessage(String queueURL, QueueMessage message, int delaySeconds) {
		SendMessageRequest send_msg_request = new SendMessageRequest()
				.withQueueUrl(queueURL)
				.withMessageBody(ObjectUtils.convertValue(message))
				.withDelaySeconds(delaySeconds);
		SendMessageResult result = sqsExtendedClient.sendMessage(send_msg_request);

		// Convertimos el resultado a un hashmap
		HashMap<String, String> resultHash = new HashMap<String, String>();
		resultHash.put("MD5OfMessageBody", result.getMD5OfMessageBody());
		resultHash.put("MessageId", result.getMessageId());
		return resultHash;
	}
	
	/**
	 *  Recibir mensajes de una cola
     *  @param queueURL la URL de la cola sobre la cual se desean recibir mensajes
     *  @return lista con los mensajes recibidos
	 */
	public ArrayList<HashMap<String, String>> receiveMessages(String queueURL) {
		List<Message> messages = sqsExtendedClient.receiveMessage(queueURL).getMessages();
		
		// Convertimos el resultado a un hashmap
		ArrayList<HashMap<String, String>> resultList = new ArrayList<HashMap<String, String>>();
		for(Message message : messages) {
            HashMap<String, String> resultItem = new HashMap<String, String>();
			resultItem.put("id", message.getMessageId());
			resultItem.put("receiptHandle", message.getReceiptHandle());
			resultItem.put("body", message.getBody());
			resultList.add(resultItem);
		}
		return resultList;
	}
	
	/**
	 *  Eliminar un mensaje de una cola
     *  @param queueURL la URL de la cola sobre la cual se desean eliminar un mensaje
     *  @param receiptHandle el handle del mensaje a ser eliminado
	 */
	public void deleteMessage(String queueURL, String receiptHandle) {
        sqsExtendedClient.deleteMessage(queueURL, receiptHandle);
	}
}