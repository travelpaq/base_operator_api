package ar.com.travelpaq.integration.commons.model;

import ar.com.travelpaq.integration.mapper.model.Accommodation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.List;

public class DataSource {
    private Connection c;
    private String url ;
    private String user;
    private String password;

    public DataSource(){
        this.url = "";
        this.user = "";
        this.password = "";
    }

    public Connection getConnection() throws Exception {
        c =  DriverManager.getConnection(this.url, this.user, this.password);
        return c;
    }

    public void setConnection(String url, String user, String password){
        this.url = url;
        this.user = user;
        this.password = password;
    }

    // Send data TO Database
    public int setData(String sql) throws Exception {
        return getConnection().createStatement().executeUpdate(sql);
    }

    // Get Data From Database
    public ResultSet query(String sql) throws Exception {
        ResultSet rs = getConnection().createStatement().executeQuery(sql);
        return rs;
    }
}
