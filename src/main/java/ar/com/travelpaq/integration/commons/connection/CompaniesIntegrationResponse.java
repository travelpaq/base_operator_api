package ar.com.travelpaq.integration.commons.connection;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;

public class CompaniesIntegrationResponse {

    @JsonProperty("data")
    private ArrayList<CompaniesIntegration> data;

    @JsonProperty("pagination")
    private Pagination pagination;

    public CompaniesIntegrationResponse() {
        this.data = new ArrayList<>();
        this.pagination = new Pagination(0, 0, 0, 0);
    }

    @JsonProperty("data")
    public ArrayList<CompaniesIntegration> getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(ArrayList<CompaniesIntegration> data) {
        this.data = data;
    }

    @JsonProperty("pagination")
    public Pagination getPagination() {
        return pagination;
    }

    @JsonProperty("pagination")
    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }
}
