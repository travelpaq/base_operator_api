package ar.com.travelpaq.integration.commons.utils;

import ar.com.travelpaq.integration.mapper.model.*;
import ar.com.travelpaq.integration.mapper.model.Package;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class PackageValidator {

    private static final String TIME24HOURS_PATTERN = "([01]?[0-9]|2[0-3]):[0-5][0-9]";
    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final String PLACE_IATA_PATTERN = "\\D{3}";
    private static List<String> validationErrors;

    public static List<String> getValidationErrors() {
        return validationErrors;
    }

    /**
     * Determina si un paquete es válido o no
     * @param _package El paquete a validar
     * @return True si el paquete es válido, false en caso contrario
     */
    public static boolean validate(Package _package) {
        validationErrors = new ArrayList<>();
        sanitizeServices(_package.getServices());
        return  validateString(_package.getTitle(), "title") &
                validateStringDate(_package.getExpirationDate(), "expiration_date") &
                validateString(_package.getExternalId(), "external_id") &
                validateDeparture(_package.getDeparture()) &
                validatePlaces(_package.getPlaces()) &
                validateCompany(_package.getCompany()) &
                validateAccomodations(_package.getAccommodations()) &
                validateAvail(_package.getAvail()) &
                validatePrices(_package.getPrices());
    }

    /**
     * Descarta los servicios del paquete que no sean válidos
     * @param services Servicios del paquete
     */
    public static void sanitizeServices(Set<Service> services) {
        for (Service service : services) {
            if(!validateString(service.getDetail()))
                services.remove(service);
        }
    }

    /**
     * Determina si la salida para el paquete es válida o no
     * @param departure La salida del paquete
     * @return True si salida es válida, false en caso contrario
     */
    public static boolean validateDeparture(Departure departure) {
        if(departure != null && validateStringDate(departure.getDate(), "departure.date") &
                validateTransportKind(departure.getTransportKind()) & validateRoutes(departure.getRoutes())) {
            return true;
        }
        else {
            return false;
        }
    }


    /**
     *
     * @param transportKind
     * @return
     */
    public static boolean validateTransportKind(String transportKind) {
        if(transportKind.equals("airline") || transportKind.equals("bus") || transportKind.equals("ship")) {
            return true;
        }
        else {
            validationErrors.add("departure.transport_kind");
            return false;
        }
    }

    /**
     *
     * @param routes
     */
    public static boolean validateRoutes(List<Frame> routes) {
        int index = 0;
        boolean result = true;
        for(Frame route : routes) {
            if(!validateRoute(route, "departure.route[" + index + "]")) {
                result = false;
            }
            index++;
        }
        return result;
    }

    /**
     *
     * @param route
     */
    public static boolean validateRoute(Frame route) throws DateTimeException {
        return  validateStringDate(route.getDepartureDate(), "route.departure_date") &
                validateStringDate(route.getArrivalDate(), "route.departure_date") &
                validateStringTime(route.getDepartureTime(), "route.departure_date") &
                validateStringTime(route.getArrivalTime(), "route.departure_date") &
                validatePlace(route.getDeparturePlace(), "route.departure_date") &
                validatePlace(route.getArrivalPlace(), "route.departure_date") &
                validateString(route.getTransportCompany().getName(), "route.departure_date") &
                validateString(route.getTravelNumber(), "route.departure_date");
    }

    private static boolean validateRoute(Frame route, String property) throws DateTimeException {
        return  validateStringDate(route.getDepartureDate(), property + ".departure_date") &
                validateStringDate(route.getArrivalDate(), property + ".arrival_date") &
                validateStringTime(route.getDepartureTime(), property + ".departure_time") &
                validateStringTime(route.getArrivalTime(), property + ".arrival_time") &
                validatePlace(route.getDeparturePlace(), property + ".departure_place") &
                validatePlace(route.getArrivalPlace(), property + ".arrival_place") &
                validateString(route.getTransportCompany().getName(), property + ".transport_company.name") &
                validateString(route.getTravelNumber(), property + ".travel_number");
    }

    /**
     *
     * @param place
     * @return
     */
    public static boolean validatePlace(Place place) {
        return validatePlaceIATA(place.getIata());
    }

    private static boolean validatePlace(Place place, String property) {
        if(Objects.nonNull(place.getIata()) && !place.getIata().isEmpty() && place.getIata().length() >= 2) {
            return true;
        }
        else {
            validationErrors.add(property);
            return false;
        }
    }

    /**
     *
     * @param iata
     * @return
     */
    public static boolean validatePlaceIATA(String iata) {
        return iata.matches(PLACE_IATA_PATTERN);
    }

    /**
     *
     * @param places
     * @return
     */
    public static boolean validatePlaces(Set<Place> places) {
        int index = 0;
        boolean result = true;
        if(places != null) {
            for(Place place : places) {
                if(!validatePlace(place, "places[" + index + "]")) {
                    result = false;
                }
                index++;
            }
        }
        else {
            validationErrors.add("places");
            result = false;
        }
        return result;
    }

    /**
     *
     * @param company
     * @return
     */
    public static boolean validateCompany(Company company) {
        if(company.getId() > 0) {
            return true;
        }
        else {
            validationErrors.add("company.id");
            return false;
        }
    }

    /**
     *
     * @param accommodations
     * @return
     */
    public static boolean validateAccomodations(List<Accommodation> accommodations) {
        int index = 0;
        boolean result = true;
        String prefix;
        if(accommodations != null) {
            for(Accommodation accommodation : accommodations) {
                prefix = "accomodations[" + index + "]";
                if(!validateStringDate(accommodation.getCheckIn(), prefix + ".check_in") ||
                        !validateStringDate(accommodation.getCheckOut(), prefix + ".check_out") ||
                        !validateString(accommodation.getRoomKind(), prefix + ".room_kind") ||
                        !validateString(accommodation.getRegime(), prefix + ".regime") ||
                        !validateHotel(accommodation.getHotel(), prefix + ".hotel")) {
                    result = false;
                }
                index++;
            }
        }
        else {
            validationErrors.add("accommodations");
            result = false;
        }
        return result;
    }

    /**
     *
     * @param hotel
     * @return
     */
    public static boolean validateHotel(Hotel hotel) {
        return validateString(hotel.getName(), "hotel.name") & validatePlace(hotel.getPlace(), "hotel.place");
    }

    private static boolean validateHotel(Hotel hotel, String property) {
        return validateString(hotel.getName(), property + ".name") & validatePlace(hotel.getPlace(), property + ".place");
    }

    /**
     *
     * @param avail
     * @return
     */
    public static boolean validateAvail(Avail avail) {
        if(avail.getRooms() > 0 && avail.getSeats() > 0) {
            return true;
        }
        else {
            validationErrors.add("avail");
            return false;
        }
    }

    /**
     *
     * @param prices
     * @return
     */
    public static boolean validatePrices(Set<Price> prices) {
        Boolean validation = true;
        for (Price price: prices){
            sanitizeChildrenPrices(price.getChildrenPrices());
            validation &= validateString(price.getCurrency(), "price.currency") &&
                    greaterThanZero(price.getBase(), "price.base") &&
                    validateTotalPrice(price.getTotalPrice());
        }
        return validation;
    }

    /**
     *
     * @param childrenPrices
     */
    public static void sanitizeChildrenPrices(List<ChildrenPrice> childrenPrices) {
        for(int i = 0;i < childrenPrices.size();i++){
            if(!(childrenPrices.get(i).getAgeFrom() >= 0) || !(childrenPrices.get(i).getAgeTo() >= 0) || !(childrenPrices.get(i).getMaxNumberChildren() > 0)
                    || !validateTotalPriceChildren(childrenPrices.get(i).getTotalPrice())) {
                childrenPrices.remove(i);
                i--;
            }
        }
    }

    /**
     *
     * @param totalPrice
     * @return
     */
    public static boolean validateTotalPriceChildren(TotalPrice totalPrice) {
        return totalPrice.getNeto() >= 0 && totalPrice.getTax() >= 0 && totalPrice.getVat() >= 0;
    }

    /**
     *
     * @param totalPrice
     * @return
     */
    public static boolean validateTotalPrice(TotalPrice totalPrice) {
        return greaterThanZero(totalPrice.getNeto(), "price.total_price.neto")
                & greaterThanZero(totalPrice.getTax(), "price.total_price.tax")
                & greaterThanZero(totalPrice.getVat(), "price.total_price.vat")
                & notNull(totalPrice.getCountryFlyTax(), "price.total_price.country_fly_tax")
                & notNull(totalPrice.getCountryLandTax(), "price.total_price.country_land_tax");
    }

    /**
     *
     * @param date
     * @return
     */
    public static boolean validateStringDate(String date) {
        try {
            LocalDate parsedDate = LocalDate.parse(date, dateFormatter);
            return true;
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            return false;
        }
    }

    private static boolean validateStringDate(String date, String property) {
        try {
            LocalDate parsedDate = LocalDate.parse(date, dateFormatter);
            return true;
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            validationErrors.add(property);
            return false;
        }
    }

    /**
     *
     * @param time
     * @return
     */
    public static boolean validateStringTime(String time) {
        return time.matches(TIME24HOURS_PATTERN);
    }

    public static boolean validateStringTime(String time, String property) {
        if(time.matches(TIME24HOURS_PATTERN)) {
            return true;
        }
        else {
            validationErrors.add(property);
            return false;
        }
    }

    /**
     *
     * @param string
     * @return
     */
    public static boolean validateString(String string) {
        return string != null && !string.equals("");
    }

    private static boolean validateString(String string, String property) {
        if(string != null && !string.equals("")) {
            return true;
        }
        else {
            validationErrors.add(property);
            return false;
        }
    }

    private static boolean greaterThanZero(float number, String property) {
        if(number > 0) {
            return true;
        }
        else {
            validationErrors.add(property);
            return false;
        }
    }

    private static boolean notNull(float number, String property) {
        if(Objects.nonNull(number)) {
            return true;
        }
        else {
            validationErrors.add(property);
            return false;
        }
    }
}
