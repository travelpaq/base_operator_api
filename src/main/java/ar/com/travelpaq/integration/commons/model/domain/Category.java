package ar.com.travelpaq.integration.commons.model.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class Category {

    @JsonProperty("name")
    private String name;

    public Category() {
        this.name = "";
    }

    public Category(String name) {
        this.name = name;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }
}
