package ar.com.travelpaq.integration.commons.connection;

public class ConnectionData {
    private String endpoint;
    private String domainPackage;
    private String wsdlPackage;
    private String user;
    private String password;
    private String id;
    private String currency;
    private Integer companyId;

    public ConnectionData(String endpoint, String domainPackage, String wsdlPackage, String user, String password, String id, String currency, Integer companyId) {
        this.endpoint = endpoint;
        this.domainPackage = domainPackage;
        this.wsdlPackage= wsdlPackage;
        this.user = user;
        this.password = password;
        this.id = id;
        this.currency = currency;
        this.companyId = companyId;
    }

    public ConnectionData() {
        this("", "", "","", "","", "", 0);
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getDomainPackage() {
        return domainPackage;
    }

    public void setDomainPackage(String domainPackage) {
        this.domainPackage = domainPackage;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getWsdlPackage() {
        return wsdlPackage;
    }

    public void setWsdlPackage(String wsdlPackage) {
        this.wsdlPackage = wsdlPackage;
    }
}
