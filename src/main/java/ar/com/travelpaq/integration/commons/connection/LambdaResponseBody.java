package ar.com.travelpaq.integration.commons.connection;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LambdaResponseBody {
    @JsonProperty("message")
    private String message;

    @JsonProperty("receiptHandle")
    private String receiptHandle;

    public LambdaResponseBody() {
       // Default constructor
    }

    public LambdaResponseBody(String message, String receiptHandle) {
        this.message = message;
        this.receiptHandle = receiptHandle;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("receiptHandle")
    public String getReceiptHandle() {
        return receiptHandle;
    }

    @JsonProperty("receiptHandle")
    public void setReceiptHandle(String receiptHandle) {
        this.receiptHandle = receiptHandle;
    }
}
