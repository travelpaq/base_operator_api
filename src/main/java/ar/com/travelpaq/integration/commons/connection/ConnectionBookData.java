package ar.com.travelpaq.integration.commons.connection;

public class ConnectionBookData {
    private String user;
    private String password;
    private String id;
    private String currency;
    private Integer operatorId;
    private Integer agencyId;
    private Float percentage_tp_ota;
    private Float percentage_tp_operator;
    private Float percentage_ota;

    public ConnectionBookData(String user, String password, String id, String currency, Integer operatorId, Integer agencyId) {
        this.user = user;
        this.password = password;
        this.id = id;
        this.currency = currency;
        this.operatorId = operatorId;
        this.agencyId = agencyId;
    }

    public ConnectionBookData() {
        this("", "", "","", 0, 0);
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public Integer getOtaId() {
        return agencyId;
    }

    public void setOtaId(Integer agencyId) {
        this.agencyId = agencyId;
    }

    public Float getPercentage_tp_ota() {
        return percentage_tp_ota;
    }

    public void setPercentage_tp_ota(Float percentage_tp_ota) {
        this.percentage_tp_ota = percentage_tp_ota;
    }

    public Float getPercentage_tp_operator() {
        return percentage_tp_operator;
    }

    public void setPercentage_tp_operator(Float percentage_tp_operator) {
        this.percentage_tp_operator = percentage_tp_operator;
    }

    public Float getPercentage_ota() {
        return percentage_ota;
    }

    public void setPercentage_ota(Float percentage_ota) {
        this.percentage_ota = percentage_ota;
    }
}
