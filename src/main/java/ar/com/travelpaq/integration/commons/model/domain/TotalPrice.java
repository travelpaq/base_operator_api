
package ar.com.travelpaq.integration.commons.model.domain;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

public class TotalPrice {

    @JsonProperty("neto")
    private Float neto;

    @JsonProperty("tax")
    private Float tax;

    @JsonProperty("vat")
    private Float vat;

    @JsonProperty("country_fly_tax")
    private Float country_fly_tax;

    @JsonProperty("country_land_tax")
    private Float country_land_tax;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public TotalPrice() {
		// Default constructor
	}

	public TotalPrice(float neto, float tax, float vat) {
		this.neto = neto;
		this.tax = tax;
		this.vat = vat;
	}

    public TotalPrice(float neto, float tax, float vat, float country_fly_tax, float country_land_tax) {
        this.neto = neto;
        this.tax = tax;
        this.vat = vat;
        this.country_fly_tax = country_fly_tax;
        this.country_land_tax = country_land_tax;
    }

    @JsonProperty("neto")
    public Float getNeto() {
        return neto;
    }

    @JsonProperty("neto")
    public void setNeto(Float neto) {
        this.neto = neto;
    }

    @JsonProperty("tax")
    public Float getTax() {
        return tax;
    }

    @JsonProperty("tax")
    public void setTax(Float tax) {
        this.tax = tax;
    }

    @JsonProperty("vat")
    public Float getVat() {
        return vat;
    }

    @JsonProperty("vat")
    public void setVat(Float vat) {
        this.vat = vat;
    }

    @JsonProperty("country_fly_tax")
    public void setCountryFlyTax(Float countryFlyTax) {
        this.country_fly_tax = countryFlyTax;
    }

    @JsonProperty("country_fly_tax")
    public Float getCountryFlyTax() {
        return country_fly_tax;
    }

    @JsonProperty("country_land_tax")
    public void setCountryLandTax(Float countryLandTax) {
        this.country_land_tax = countryLandTax;
    }

    @JsonProperty("country_land_tax")
    public Float getCountryLandTax() {
        return country_land_tax;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
