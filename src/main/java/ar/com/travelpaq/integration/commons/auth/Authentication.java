package ar.com.travelpaq.integration.commons.auth;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;

public class Authentication {

    public static Boolean checkSuperAdmin(String token){
        String []token_parts = token.split(" ");

        if(token_parts.length != 2)
            return false;

        token = token_parts[1];

        try {
            DecodedJWT jwt = JWT.decode(token);
            if(Integer.parseInt(jwt.getClaim("i").asString()) == 72){
                return true;
            } else {
                return false;
            }
        } catch (JWTDecodeException e){
            e.printStackTrace(System.out);
            return false;
        }
    }
}
