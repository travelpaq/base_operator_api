
package ar.com.travelpaq.integration.commons.model.domain;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

public class Accommodation {

    @JsonProperty("number_nights")
    private int number_nights;

    @JsonProperty("check_in")
    private String check_in;

    @JsonProperty("check_out")
    private String check_out;

    @JsonProperty("regime")
    private Regime regime;

    @JsonProperty("room_kind")
    private RoomKind room_kind;

    @JsonProperty("hotel")
    private Hotel hotel;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Accommodation() {
		regime = new Regime();
        room_kind= new RoomKind();
        hotel = new Hotel();
	}

	public Accommodation(int number_nights, String check_in, String check_out, RoomKind room_kind, Regime regime, Hotel hotel) {
		this.number_nights = number_nights;
		this.check_in = check_in;
		this.check_out = check_out;
		this.room_kind = room_kind;
		this.regime = regime;
		this.hotel = hotel;
	}

    @JsonProperty("number_nights")
    public int getNumberNights() {
        return number_nights;
    }

    @JsonProperty("number_nights")
    public void setNumberNights(int numberNights) {
        this.number_nights = numberNights;
    }

    @JsonProperty("check_in")
    public String getCheckIn() {
        return check_in;
    }

    @JsonProperty("check_in")
    public void setCheckIn(String checkIn) {
        this.check_in = checkIn;
    }

    @JsonProperty("check_out")
    public String getCheckOut() {
        return check_out;
    }

    @JsonProperty("check_out")
    public void setCheckOut(String checkOut) {
        this.check_out = checkOut;
    }

    @JsonProperty("room_kind")
    public RoomKind getRoom_kind() {
        return room_kind;
    }

    @JsonProperty("room_kind")
    public void setRoom_kind(RoomKind room_kind) {
        this.room_kind = room_kind;
    }

    @JsonProperty("regime")
    public Regime getRegime() {
        return regime;
    }

    @JsonProperty("regime")
    public void setRegime(Regime regime) {
        this.regime = regime;
    }

    @JsonProperty("hotel")
    public Hotel getHotel() {
        return hotel;
    }

    @JsonProperty("hotel")
    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
