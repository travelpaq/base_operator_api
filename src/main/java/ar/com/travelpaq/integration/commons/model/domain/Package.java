
package ar.com.travelpaq.integration.commons.model.domain;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Package {

    @JsonProperty("id")
    private String id;

    @JsonProperty("external_id")
    private String externalId;

    @JsonProperty("title")
    private String title;

    @JsonProperty("observations")
    private String observations;

    @JsonProperty("categories")
    private List<Category> categories;

    @JsonProperty("services")
    private Set<Service> services;

    @JsonProperty("departure")
    private Departure departure;

    @JsonProperty("accommodations")
    private List<Accommodation> accommodations;

    @JsonProperty("total_nights")
    private int totalNights;

    @JsonProperty("places")
    private Set<Place> places;

    @JsonProperty("price")
    private Price price;

    @JsonProperty("avail")
    private Avail avail;

    @JsonProperty("company")
    private Company company;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Package() {
		// Default constructor
	}

	public Package(String externalId, String title, String observations, int totalNights,
                   List<Accommodation> accommodations, Avail avail, Departure departure,
                   Set<Place> places, Price price, Set<Service> services) {
		this.externalId = externalId;
		this.title = title;
		this.observations = observations;
		this.totalNights = totalNights;
		this.accommodations = accommodations;
		this.avail = avail;
		this.departure = departure;
		this.places = places;
		this.price = price;
		this.services = services;
	}

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("observations")
    public String getObservations() {
        return observations;
    }

    @JsonProperty("observations")
    public void setObservations(String observations) {
        this.observations = observations;
    }

    @JsonProperty("categories")
    public List<Category> getCategories() {
        return categories;
    }

    @JsonProperty("categories")
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @JsonProperty("total_nights")
    public int getTotalNights() {
        return totalNights;
    }

    @JsonProperty("total_nights")
    public void setTotalNights(int totalNights) {
        this.totalNights = totalNights;
    }

    @JsonProperty("external_id")
    public String getExternalId() {
        return externalId;
    }

    @JsonProperty("external_id")
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @JsonProperty("services")
    public Set<Service> getServices() {
        return services;
    }

    @JsonProperty("services")
    public void setServices(Set<Service> services) {
        this.services = services;
    }

    @JsonProperty("departure")
    public Departure getDeparture() {
        return departure;
    }

    @JsonProperty("departure")
    public void setDeparture(Departure departure) {
        this.departure = departure;
    }

    @JsonProperty("places")
    public Set<Place> getPlaces() {
        return places;
    }

    @JsonProperty("places")
    public void setPlaces(Set<Place> place) {
        this.places = place;
    }

    @JsonProperty("company")
    public Company getCompany() {
        return company;
    }

    @JsonProperty("company")
    public void setCompany(Company company) {
        this.company = company;
    }

    @JsonProperty("accommodations")
    public List<Accommodation> getAccommodations() {
        return accommodations;
    }

    @JsonProperty("accommodations")
    public void setAccommodations(List<Accommodation> accommodations) {
        this.accommodations = accommodations;
    }

    @JsonProperty("avail")
    public Avail getAvail() {
        return avail;
    }

    @JsonProperty("avail")
    public void setAvail(Avail avail) {
        this.avail = avail;
    }

    @JsonProperty("price")
    public Price getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Price price) {
        this.price = price;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
