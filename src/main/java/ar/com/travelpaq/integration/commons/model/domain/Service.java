
package ar.com.travelpaq.integration.commons.model.domain;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

public class Service {

    @JsonProperty("detail")
    private String detail;

    @JsonProperty("service_kind")
    private ServiceKind serviceKind;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Service() {
		serviceKind = new ServiceKind();
	}

	public Service(String detail) {
		this.detail = detail;
	}

    @JsonProperty("detail")
    public String getDetail() {
        return detail;
    }

    @JsonProperty("detail")
    public void setDetail(String detail) {
        this.detail = detail;
    }

    @JsonProperty("service_kind")
    public ServiceKind getServiceKind() {
        return serviceKind;
    }

    @JsonProperty("service_kind")
    public void setServiceKind(ServiceKind serviceKind) {
        this.serviceKind = serviceKind;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
