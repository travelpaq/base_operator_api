package ar.com.travelpaq.integration.commons.connection;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Agreement {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("operator_id")
    private Integer operator_id;

    @JsonProperty("agency_id")
    private Integer agency_id;

    @JsonProperty("expired_date")
    private String expired_date;

    @JsonProperty("percentage_ota")
    private Float percentage_ota;

    @JsonProperty("percentage_tp_ota")
    private Float percentage_tp_ota;

    @JsonProperty("created")
    private String created;

    @JsonProperty("percentage_tp_operator")
    private Float percentage_tp_operator;

    @JsonProperty("modified")
    private String modified;

    @JsonProperty("backoffice_user")
    private String backoffice_user;

    @JsonProperty("backoffice_password")
    private String backoffice_password;

    @JsonProperty("backoffice_id")
    private String backoffice_id;

    @JsonProperty("backoffice_user_test")
    private String backoffice_user_test;

    @JsonProperty("backoffice_password_test")
    private String backoffice_password_test;

    @JsonProperty("backoffice_id_test")
    private String backoffice_id_test;

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("operator_id")
    public Integer getOperator_id() {
        return operator_id;
    }

    @JsonProperty("operator_id")
    public void setOperator_id(Integer operator_id) {
        this.operator_id = operator_id;
    }

    @JsonProperty("agency_id")
    public Integer getAgency_id() {
        return agency_id;
    }

    @JsonProperty("agency_id")
    public void setAgency_id(Integer agency_id) {
        this.agency_id = agency_id;
    }

    @JsonProperty("expired_date")
    public String getExpired_date() {
        return expired_date;
    }

    @JsonProperty("expired_date")
    public void setExpired_date(String expired_date) {
        this.expired_date = expired_date;
    }

    @JsonProperty("percentage_ota")
    public Float getPercentage_ota() {
        return percentage_ota;
    }

    @JsonProperty("percentage_ota")
    public void setPercentage_ota(Float percentage_ota) {
        this.percentage_ota = percentage_ota;
    }

    @JsonProperty("percentage_tp_ota")
    public Float getPercentage_tp_ota() {
        return percentage_tp_ota;
    }

    @JsonProperty("percentage_tp_ota")
    public void setPercentage_tp_ota(Float percentage_tp_ota) {
        this.percentage_tp_ota = percentage_tp_ota;
    }

    @JsonProperty("created")
    public String getCreated() {
        return created;
    }

    @JsonProperty("created")
    public void setCreated(String created) {
        this.created = created;
    }

    @JsonProperty("percentage_tp_operator")
    public Float getPercentage_tp_operator() {
        return percentage_tp_operator;
    }

    @JsonProperty("percentage_tp_operator")
    public void setPercentage_tp_operator(Float percentage_tp_operator) {
        this.percentage_tp_operator = percentage_tp_operator;
    }

    @JsonProperty("modified")
    public String getModified() {
        return modified;
    }

    @JsonProperty("modified")
    public void setModified(String modified) {
        this.modified = modified;
    }

    @JsonProperty("backoffice_user")
    public String getBackoffice_user() {
        return backoffice_user;
    }

    @JsonProperty("backoffice_user")
    public void setBackoffice_user(String backoffice_user) {
        this.backoffice_user = backoffice_user;
    }

    @JsonProperty("backoffice_password")
    public String getBackoffice_password() {
        return backoffice_password;
    }

    @JsonProperty("backoffice_password")
    public void setBackoffice_password(String backoffice_password) {
        this.backoffice_password = backoffice_password;
    }

    @JsonProperty("backoffice_id")
    public String getBackoffice_id() {
        return backoffice_id;
    }

    @JsonProperty("backoffice_id")
    public void setBackoffice_id(String backoffice_id) {
        this.backoffice_id = backoffice_id;
    }

    @JsonProperty("backoffice_user_test")
    public String getBackoffice_user_test() {
        return backoffice_user_test;
    }

    @JsonProperty("backoffice_user_test")
    public void setBackoffice_user_test(String backoffice_user_test) {
        this.backoffice_user_test = backoffice_user_test;
    }

    @JsonProperty("backoffice_password_test")
    public String getBackoffice_password_test() {
        return backoffice_password_test;
    }

    @JsonProperty("backoffice_password_test")
    public void setBackoffice_password_test(String backoffice_password_test) {
        this.backoffice_password_test = backoffice_password_test;
    }

    @JsonProperty("backoffice_id_test")
    public String getBackoffice_id_test() {
        return backoffice_id_test;
    }

    @JsonProperty("backoffice_id_test")
    public void setBackoffice_id_test(String backoffice_id_test) {
        this.backoffice_id_test = backoffice_id_test;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
