
package ar.com.travelpaq.integration.commons.model.domain;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Price {

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("origin_currency")
    private String origin_currency;

    @JsonProperty("price_per_person")
    private float pricePerPerson;

    @JsonProperty("final_price")
    private float finalPrice;

    @JsonProperty("markup")
    private float markup;

    @JsonProperty("label")
    private String label;

    @JsonProperty("change")
    private float change;

    @JsonProperty("agency_commission")
    private float agencyCommission;

    @JsonProperty("total_price")
    private TotalPrice totalPrice;

    @JsonProperty("room_prices")
    private List<RoomPrice> roomsPrice;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Price() {
		totalPrice = new TotalPrice();
		roomsPrice = new ArrayList<>();
	}


    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("origin_currency")
    public String getOriginCurrency() {
        return origin_currency;
    }

    @JsonProperty("origin_currency")
    public void setOriginCurrency(String origin_currency) {
        this.origin_currency = origin_currency;
    }

    @JsonProperty("price_per_person")
    public float getPricePerPerson() {
        return pricePerPerson;
    }

    @JsonProperty("price_per_person")
    public void setPricePerPerson(float pricePerPerson) {
        this.pricePerPerson = pricePerPerson;
    }

    @JsonProperty("final_price")
    public float getFinalPrice() {
        return finalPrice;
    }

    @JsonProperty("final_price")
    public void setFinalPrice(float finalPrice) {
        this.finalPrice = finalPrice;
    }

    @JsonProperty("markup")
    public float getMarkup() {
        return markup;
    }

    @JsonProperty("markup")
    public void setMarkup(float markup) {
        this.markup = markup;
    }

    @JsonProperty("label")
    public String getLabel() {
        return label;
    }

    @JsonProperty("label")
    public void setLabel(String label) {
        this.label = label;
    }

    @JsonProperty("change")
    public float getChange() {
        return change;
    }

    @JsonProperty("change")
    public void setChange(float change) {
        this.change = change;
    }

    @JsonProperty("agency_commission")
    public float getAgencyCommission() {
        return agencyCommission;
    }

    @JsonProperty("agency_commission")
    public void setAgencyCommission(float agencyCommission) {
        this.agencyCommission = agencyCommission;
    }

    @JsonProperty("total_price")
    public TotalPrice getTotalPrice() {
        return totalPrice;
    }

    @JsonProperty("total_price")
    public void setTotalPrice(TotalPrice totalPrice) {
        this.totalPrice = totalPrice;
    }

    @JsonProperty("rooms_prices")
    public List<RoomPrice> getRoomsPrice() {
        return roomsPrice;
    }

    @JsonProperty("rooms_prices")
    public void setRoomsPrice(List<RoomPrice> roomsPrice) {
        this.roomsPrice = roomsPrice;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
