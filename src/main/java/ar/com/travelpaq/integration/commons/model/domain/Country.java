package ar.com.travelpaq.integration.commons.model.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Country {

    @JsonProperty("name")
    private String name;
    @JsonProperty("iata")
    private String iata;
    @JsonProperty("region")
    private Region region;

    public Country() {
        region = new Region();
    }

    public Country(String name, String iata, Region regions) {
        this.name = name;
        this.iata = iata;
        this.region = regions;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("iata")
    public String getIata() {
        return iata;
    }

    @JsonProperty("iata")
    public void setIata(String iata) {
        this.iata = iata;
    }

    @JsonProperty("region")
    public Region getRegion() {
        return region;
    }

    @JsonProperty("region")
    public void setRegion(Region regions) {
        this.region = regions;
    }
}
