package ar.com.travelpaq.integration.commons.connection;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;

public class AgreementResponse {

    @JsonProperty("data")
    private ArrayList<Agreement> data;

    public AgreementResponse() {
        this.data = new ArrayList<Agreement>();
    }

    @JsonProperty("data")
    public ArrayList<Agreement> getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(ArrayList<Agreement> data) {
        this.data = data;
    }

}
