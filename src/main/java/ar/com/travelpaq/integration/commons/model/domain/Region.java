package ar.com.travelpaq.integration.commons.model.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Region {

    @JsonProperty("name")
    private String name;
    @JsonProperty("iata")
    private String iata;

    public Region() {
    }

    public Region(String name, String iata) {
        this.name = name;
        this.iata = iata;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("iata")
    public String getIata() {
        return iata;
    }

    @JsonProperty("iata")
    public void setIata(String iata) {
        this.iata = iata;
    }
}
