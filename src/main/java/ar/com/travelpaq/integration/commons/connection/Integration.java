package ar.com.travelpaq.integration.commons.connection;

import ar.com.travelpaq.integration.commons.configuration.ExecutionMode;
import ar.com.travelpaq.integration.commons.utils.ObjectUtils;
import okhttp3.OkHttpClient;
import okhttp3.Request;

import java.io.IOException;

public class Integration {

    private static String travelpaqAccessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpIjoiNzIiLCJjb21wYW55X3R5cGUiOiJvcGVyYXRvciJ9.-Lp9A4Mn3eQ7WCtVrM2dfqXuDhFWlSbiv-cCgHYOxsI";
    private static String integrationApiEndpoint = System.getenv("DB_ENDPOINT_TRAVELPAQ");//"http://travelpaq-integration.us-east-2.elasticbeanstalk.com/";
    private static OkHttpClient client = new OkHttpClient();

    public static ConnectionData getConnectionData(int companyId, ExecutionMode executionMode) throws IOException {

        ConnectionData connectionData = new ConnectionData();
        Request request = new Request.Builder().addHeader("Authorization", "Bearer " + travelpaqAccessToken)
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "application/json")
                .url(integrationApiEndpoint + "companies_integrations?company_id=" + companyId)
                .build();
        String stringResponse = client.newCall(request).execute().body().string();
        CompaniesIntegrationResponse response = ObjectUtils.convertValue(stringResponse, CompaniesIntegrationResponse.class);

        if(response.getData().size() > 0){

            CompaniesIntegration companiesIntegration = response.getData().get(0);
            connectionData.setDomainPackage(companiesIntegration.getDomain_package());
            connectionData.setWsdlPackage(companiesIntegration.getWsdl_package());
            connectionData.setCurrency(companiesIntegration.getCurrency());

            if (executionMode == ExecutionMode.PROD){
                connectionData.setEndpoint(companiesIntegration.getUrl());
                connectionData.setId(companiesIntegration.getBackoffice_id());
                connectionData.setUser(companiesIntegration.getBackoffice_user());
                connectionData.setPassword(companiesIntegration.getBackoffice_password());
            } else {
                connectionData.setEndpoint(companiesIntegration.getUrl_test());
                connectionData.setId(companiesIntegration.getBackoffice_id_test());
                connectionData.setUser(companiesIntegration.getBackoffice_user_test());
                connectionData.setPassword(companiesIntegration.getBackoffice_password_test());
            }
            return connectionData;
        } else {
            return null;
        }
    }

    public static ConnectionBookData getBookConnectionData(Integer agencyId, Integer operatorId, ExecutionMode executionMode) throws IOException {

        ConnectionBookData connectionBookData = new ConnectionBookData();

        Request request = new Request.Builder().addHeader("Authorization", "Bearer " + travelpaqAccessToken)
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "application/json")
                .url(integrationApiEndpoint + "agreements?agency_id=" + agencyId+ "&operator_id=" + operatorId)
                .build();

        String stringResponse = client.newCall(request).execute().body().string();
        AgreementResponse response = ObjectUtils.convertValue(stringResponse, AgreementResponse.class);

        if(response.getData().size() > 0){
            Agreement agreementResponse = response.getData().get(0);
            connectionBookData.setCurrency(agreementResponse.getCurrency());
            connectionBookData.setOperatorId(agreementResponse.getOperator_id());
            connectionBookData.setOtaId(agreementResponse.getAgency_id());
            connectionBookData.setPercentage_tp_ota(agreementResponse.getPercentage_tp_ota());
            connectionBookData.setPercentage_tp_operator(agreementResponse.getPercentage_tp_operator());
            connectionBookData.setPercentage_ota(agreementResponse.getPercentage_ota());

            if (executionMode == ExecutionMode.PROD){
                connectionBookData.setId(agreementResponse.getBackoffice_id());
                connectionBookData.setUser(agreementResponse.getBackoffice_user());
                connectionBookData.setPassword(agreementResponse.getBackoffice_password());
            }else {
                connectionBookData.setId(agreementResponse.getBackoffice_id_test());
                connectionBookData.setUser(agreementResponse.getBackoffice_user_test());
                connectionBookData.setPassword(agreementResponse.getBackoffice_password_test());
            }
            return connectionBookData;
        } else {
            return null;
        }
    }
}
