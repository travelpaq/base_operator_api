package ar.com.travelpaq.integration.commons.configuration;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;

import org.springframework.util.FileCopyUtils;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class GoogleCredentialsFile {
    public GoogleCredentialsFile () {}

    public void checkFile() {
        File tempDir = new File("/home/ec2-user/packages-267220-af391d9b3c9d.json");

        if (!tempDir.exists()) {

            //OBTENGO CREDENCIALES DEL BUCKET EN AWS
            Regions region = Regions.fromName("us-east-2");
            AWSCredentialsProvider credentialsProvider = new DefaultAWSCredentialsProviderChain();
            AmazonS3 s3client = (AmazonS3)((AmazonS3ClientBuilder)((AmazonS3ClientBuilder) AmazonS3Client.builder().withCredentials(credentialsProvider)).withRegion(region)).build();

            S3Object s3Object = s3client.getObject("google-application", "packages-267220-af391d9b3c9d.json");
            S3ObjectInputStream inputStream = s3Object.getObjectContent();

            try {
                FileCopyUtils.copy(inputStream, new FileOutputStream("/home/ec2-user/packages-267220-af391d9b3c9d.json"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
