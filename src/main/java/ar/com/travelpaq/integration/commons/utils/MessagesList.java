package ar.com.travelpaq.integration.commons.utils;

public class MessagesList {

    /*** Messages errors list ****/

    //---------  Booking  -----------
    public static String BOOKING_BUILD_FARE_ERROR = "Lo sentimos! Se produjo un error al armar la tarifa de la reserva.";
    public static String BOOKING_BUILD_ROOM = "Lo sentimos! Se produjo un error al armar las habitaciones de la reserva.";
    public static String BOOKING_PREV_STEP = "Lo sentimos! Se produjo un error al prepar la reserva.";
    public static String BOOKING_SUPPLIER_BOOKING = "Lo sentimos! Se produjo un error al relizar la reserva.";
    public static String BOOKING_GET_PRICING = "Lo sentimos! Se produjo un error al relizar la reserva.";
    //---------  Booking  -----------

    //---------  CheckAvail  -----------
    public static String AVAILABILITY_BAD_PARAMETERS = "Lo sentimos! Los datos para la comprobación de disponibilidad no son corractos.";
    //---------  CheckAvail  -----------
}
