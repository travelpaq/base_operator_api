package ar.com.travelpaq.integration.commons.utils;

public enum RegionName {

    US_EAST_1("us-east-1","Virginia"),
    US_EAST_2("us-east-2","Ohio");

    private final String region;
    private final String description;

    RegionName(String region, String description) {
        this.region = region;
        this.description = description;
    }

    public String getRegion(){ return this.region; }
    public String getDescription(){ return this.description; }

}
