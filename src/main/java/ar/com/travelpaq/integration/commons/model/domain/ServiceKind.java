package ar.com.travelpaq.integration.commons.model.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ServiceKind {

    @JsonProperty("detail")
    private String detail;

    @JsonProperty("icon")
    private String icon;

    public ServiceKind() {
    }

    public ServiceKind(String detail, String icon) {
        this.detail = detail;
        this.icon = icon;
    }

    @JsonProperty("detail")
    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @JsonProperty("icon")
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
