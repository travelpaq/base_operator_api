package ar.com.travelpaq.integration.commons.model.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Image {

    @JsonProperty("picture")
    private String picture;
    @JsonProperty("thumbnail")
    private String thumbnail;

    public Image() {
    }

    public Image(String picture, String thumbnail) {
        this.picture = picture;
        this.thumbnail = thumbnail;
    }

    @JsonProperty("picture")
    public String getPicture() {
        return picture;
    }

    @JsonProperty("picture")
    public void setPicture(String picture) {
        this.picture = picture;
    }

    @JsonProperty("thumbnail")
    public String getThumbnail() {
        return thumbnail;
    }

    @JsonProperty("thumbnail")
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
