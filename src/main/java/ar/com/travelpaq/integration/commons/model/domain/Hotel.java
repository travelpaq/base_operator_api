
package ar.com.travelpaq.integration.commons.model.domain;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Hotel {

    @JsonProperty("star_rating")
    private float starRating;

    @JsonProperty("name")
    private String name;

    @JsonProperty("address")
    private String address;

    @JsonProperty("url")
    private String url;

    @JsonProperty("lat")
    private String latitude;

    @JsonProperty("lon")
    private String longitude;

    @JsonProperty("tripadvisor_rating")
    private float tripadvisorRating;

    @JsonProperty("popularity")
    private int popularity;

    @JsonProperty("description")
    private String description;

    @JsonProperty("place")
    private Place place;

    @JsonProperty("images")
    private List<Image> images;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Hotel() {
		this.images = new ArrayList<>();
        this.place = new Place();
	}

	public Hotel(String name, Place place) {
		this.name = name;
		this.place = place;
	}

    public Hotel(float starRating, String name, String address, String url, String latitude, String longitude,
                 float tripadvisorRating, int popularity, String description, Place place, List<Image> images) {
        this.starRating = starRating;
        this.name = name;
        this.address = address;
        this.url = url;
        this.latitude = latitude;
        this.longitude = longitude;
        this.tripadvisorRating = tripadvisorRating;
        this.popularity = popularity;
        this.description = description;
        this.place = place;
        this.images = images;
    }


    @JsonProperty("star_rating")
    public float getStarRating() {
        return starRating;
    }

    @JsonProperty("star_rating")
    public void setStarRating(float starRating) {
        this.starRating = starRating;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty("lat")
    public String getLatitude() {
        return latitude;
    }

    @JsonProperty("lat")
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @JsonProperty("lng")
    public String getLongitude() {
        return longitude;
    }

    @JsonProperty("lng")
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @JsonProperty("tripadvisor_rating")
    public float getTripadvisorRating() {
        return tripadvisorRating;
    }

    @JsonProperty("tripadvisor_rating")
    public void setTripadvisorRating(float tripadvisorRating) {
        this.tripadvisorRating = tripadvisorRating;
    }

    @JsonProperty("popularity")
    public int getPopularity() {
        return popularity;
    }

    @JsonProperty("popularity")
    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("place")
    public Place getPlace() {
        return place;
    }

    @JsonProperty("place")
    public void setPlace(Place place) {
        this.place = place;
    }

    @JsonProperty("images")
    public List<Image> getImages() {
        if(images == null)
            images = new ArrayList<>();
        return images;
    }

    @JsonProperty("images")
    public void setImages(List<Image> images) {
        this.images = images;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
