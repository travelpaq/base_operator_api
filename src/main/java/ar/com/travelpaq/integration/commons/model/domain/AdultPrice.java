package ar.com.travelpaq.integration.commons.model.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AdultPrice {

    @JsonProperty("adult")
    private int adult;
    @JsonProperty("final_price")
    private float finalPrice;
    @JsonProperty("total_price")
    private TotalPrice totalPrice;

    public AdultPrice() {
        this.totalPrice = new TotalPrice();
    }

    public AdultPrice(int adult, float finalPrice, TotalPrice totalPrice) {
        this.adult = adult;
        this.finalPrice = finalPrice;
        this.totalPrice = totalPrice;
    }

    @JsonProperty("adult")
    public int getAdult() {
        return adult;
    }

    @JsonProperty("adult")
    public void setAdult(int adult) {
        this.adult = adult;
    }

    @JsonProperty("final_price")
    public float getFinalPrice() {
        return finalPrice;
    }

    @JsonProperty("final_price")
    public void setFinalPrice(float finalPrice) {
        this.finalPrice = finalPrice;
    }

    @JsonProperty("total_price")
    public TotalPrice getTotalPrice() {
        return totalPrice;
    }

    @JsonProperty("total_price")
    public void setTotalPrice(TotalPrice totalPrice) {
        this.totalPrice = totalPrice;
    }
}
