package ar.com.travelpaq.integration.commons.model.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RoomKind {
    @JsonProperty("id")
    private int id;
    @JsonProperty("name")
    private String name;

    public RoomKind() {
    }

    @JsonProperty("id")
    public int getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }
}
