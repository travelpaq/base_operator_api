package ar.com.travelpaq.integration.commons.connection;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

public class CompaniesIntegration {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("url")
    private String url;

    @JsonProperty("url_test")
    private String url_test;

    @JsonProperty("backoffice_user")
    private String backoffice_user;

    @JsonProperty("backoffice_password")
    private String backoffice_password;

    @JsonProperty("backoffice_id")
    private String backoffice_id;

    @JsonProperty("backoffice_user_test")
    private String backoffice_user_test;

    @JsonProperty("backoffice_password_test")
    private String backoffice_password_test;

    @JsonProperty("backoffice_id_test")
    private String backoffice_id_test;

    @JsonProperty("host")
    private String host;

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("company_id")
    private Integer company_id;

    @JsonProperty("integration_id")
    private Integer integration_id;

    @JsonProperty("host_test")
    private String host_test;

    @JsonProperty("deleted")
    private Boolean deleted;

    @JsonProperty("domain_package")
    private String domain_package;

    @JsonProperty("wsdl_package")
    private String wsdl_package;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public CompaniesIntegration() {
        super();
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty("url_test")
    public String getUrl_test() {
        return url_test;
    }

    @JsonProperty("url_test")
    public void setUrl_test(String url_test) {
        this.url_test = url_test;
    }

    @JsonProperty("backoffice_user")
    public String getBackoffice_user() {
        return backoffice_user;
    }

    @JsonProperty("backoffice_user")
    public void setBackoffice_user(String backoffice_user) {
        this.backoffice_user = backoffice_user;
    }

    @JsonProperty("backoffice_password")
    public String getBackoffice_password() {
        return backoffice_password;
    }

    @JsonProperty("backoffice_password")
    public void setBackoffice_password(String backoffice_password) {
        this.backoffice_password = backoffice_password;
    }

    @JsonProperty("backoffice_id")
    public String getBackoffice_id() {
        return backoffice_id;
    }

    @JsonProperty("backoffice_id")
    public void setBackoffice_id(String backoffice_id) {
        this.backoffice_id = backoffice_id;
    }

    @JsonProperty("backoffice_user_test")
    public String getBackoffice_user_test() {
        return backoffice_user_test;
    }

    @JsonProperty("backoffice_user_test")
    public void setBackoffice_user_test(String backoffice_user_test) {
        this.backoffice_user_test = backoffice_user_test;
    }

    @JsonProperty("backoffice_password_test")
    public String getBackoffice_password_test() {
        return backoffice_password_test;
    }

    @JsonProperty("backoffice_password_test")
    public void setBackoffice_password_test(String backoffice_password_test) {
        this.backoffice_password_test = backoffice_password_test;
    }

    @JsonProperty("backoffice_id_test")
    public String getBackoffice_id_test() {
        return backoffice_id_test;
    }

    @JsonProperty("backoffice_id_test")
    public void setBackoffice_id_test(String backoffice_id_test) {
        this.backoffice_id_test = backoffice_id_test;
    }

    @JsonProperty("host")
    public String getHost() {
        return host;
    }

    @JsonProperty("host")
    public void setHost(String host) {
        this.host = host;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("company_id")
    public Integer getCompany_id() {
        return company_id;
    }

    @JsonProperty("company_id")
    public void setCompany_id(Integer company_id) {
        this.company_id = company_id;
    }

    @JsonProperty("integration_id")
    public Integer getIntegration_id() {
        return integration_id;
    }

    @JsonProperty("integration_id")
    public void setIntegration_id(Integer integration_id) {
        this.integration_id = integration_id;
    }

    @JsonProperty("host_test")
    public String getHost_test() {
        return host_test;
    }

    @JsonProperty("host_test")
    public void setHost_test(String host_test) {
        this.host_test = host_test;
    }

    @JsonProperty("deleted")
    public Boolean getDeleted() {
        return deleted;
    }

    @JsonProperty("deleted")
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @JsonProperty("domain_package")
    public String getDomain_package() {
        return domain_package;
    }

    @JsonProperty("domain_package")
    public void setDomain_package(String domain_package) {
        this.domain_package = domain_package;
    }

    @JsonProperty("wsdl_package")
    public String getWsdl_package() {
        return wsdl_package;
    }

    @JsonProperty("wsdl_package")
    public void setWsdl_package(String wsdl_package) {
        this.wsdl_package = wsdl_package;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }
}
