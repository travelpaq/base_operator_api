
package ar.com.travelpaq.integration.commons.model.domain;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Frame {

    @JsonProperty("travel_part")
    private String travel_part;

    @JsonProperty("travel_duration")
    private String travel_duration;

    @JsonProperty("travel_number")
    private String travel_number;

    @JsonProperty("arrival_date")
    private String arrival_date;

    @JsonProperty("arrival_time")
    private String arrival_time;

    @JsonProperty("departure_date")
    private String departure_date;

    @JsonProperty("departure_time")
    private String departure_time;

    @JsonProperty("departure_place")
    private Place departure_place;

    @JsonProperty("arrival_place")
    private Place arrival_place;

    @JsonProperty("transport_company")
    private TransportCompany transport_company;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Frame() {
		departure_place = new Place();
		arrival_place = new Place();
		transport_company = new TransportCompany();
	}

    public Frame(String travel_number, String arrival_date, String arrival_time, String departure_date,
                 String departure_time, Place departure_place, Place arrival_place, TransportCompany transport_company) {
        this.travel_number = travel_number;
        this.arrival_date = arrival_date;
        this.arrival_time = arrival_time;
        this.departure_date = departure_date;
        this.departure_time = departure_time;
        this.departure_place = departure_place;
        this.arrival_place = arrival_place;
        this.transport_company = transport_company;
    }

    @JsonProperty("travel_part")
    public String getTravelPart() {
        return travel_part;
    }

    @JsonProperty("travel_part")
    public void setTravelPart(String travel_part) {
        this.travel_part = travel_part;
    }

    @JsonProperty("travel_duration")
    public String getTravelDuration() {
        return travel_duration;
    }

    @JsonProperty("travel_duration")
    public void setTravelDuration(String travel_duration) {
        this.travel_duration = travel_duration;
    }

    @JsonProperty("travel_number")
    public String getTravelNumber() {
        return travel_number;
    }

    @JsonProperty("travel_number")
    public void setTravelNumber(String travel_number) {
        this.travel_number = travel_number;
    }

    @JsonProperty("arrival_time")
    public String getArrivalTime() {
        return arrival_time;
    }

    @JsonProperty("arrival_time")
    public void setArrivalTime(String arrival_time) {
        this.arrival_time = arrival_time;
    }

    @JsonProperty("arrival_date")
    public String getArrivalDate() {
        return arrival_date;
    }

    @JsonProperty("arrival_date")
    public void setArrivalDate(String arrival_date) {
        this.arrival_date = arrival_date;
    }

    @JsonProperty("departure_time")
    public String getDepartureTime() {
        return departure_time;
    }

    @JsonProperty("departure_time")
    public void setDepartureTime(String departure_time) {
        this.departure_time = departure_time;
    }

    @JsonProperty("departure_date")
    public String getDepartureDate() {
        return departure_date;
    }

    @JsonProperty("departure_date")
    public void setDepartureDate(String departure_date) {
        this.departure_date = departure_date;
    }

    @JsonProperty("departures_place")
    public Place getDeparturePlace() {
        return departure_place;
    }

    @JsonProperty("departures_place")
    public void setDeparturePlace(Place departure_place) {
        this.departure_place = departure_place;
    }

    @JsonProperty("arrival_place")
    public Place getArrivalPlace() {
        return arrival_place;
    }

    @JsonProperty("arrival_place")
    public void setArrivalPlace(Place arrival_place) {
        this.arrival_place = arrival_place;
    }

    @JsonProperty("transport_company")
    public TransportCompany getTransportCompany() {
        return transport_company;
    }

    @JsonProperty("transport_company")
    public void setTransportCompany(TransportCompany transport_company) {
        this.transport_company = transport_company;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
