package ar.com.travelpaq.integration.commons.model.domain;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

public class Company {

    @JsonProperty("name")
    private String name;
    @JsonProperty("cuit")
    private String cuit;
    @JsonProperty("id")
    private int id;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Company() {
        // Default constructor
    }

    public Company(String name, String cuit, int id) {
        this.name = name;
        this.cuit = cuit;
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("cuit")
    public String getCuit() {
        return cuit;
    }

    @JsonProperty("cuit")
    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    @JsonProperty("id")
    public int getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}