package ar.com.travelpaq.integration.commons.configuration;

public enum ExecutionMode {

    DEV, PROD, SANDBOX, STAGE;

    public static ExecutionMode fromString(String string) throws Exception {
        switch (string){
            case "DEV":
                return ExecutionMode.DEV;

            case "PROD":
                return ExecutionMode.PROD;

            case "SANDBOX":
                return ExecutionMode.SANDBOX;

            case "STAGE":
                return ExecutionMode.STAGE;

            default:
                return ExecutionMode.DEV;
        }
    }
}
