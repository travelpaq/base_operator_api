package ar.com.travelpaq.integration.commons.utils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QueueMessage {

    @JsonProperty("data")
    private String data;

    @JsonProperty("operatorId")
    private String operatorId;

    @JsonProperty("queueName")
    private String queueName;

    @JsonProperty("regionQueueName")
    private String regionQueueName;

    @JsonProperty("receiptHandle")
    private String receiptHandle;

    @JsonProperty("forwarded")
    private boolean forwarded;

    @JsonProperty("destination")
    private String destination;

    @JsonProperty("dateFrom")
    private String dateFrom;

    @JsonProperty("dateTo")
    private String dateTo;

    @JsonProperty("hash")
    private String hash;

    @JsonProperty("group")
    private String group;

    @JsonProperty("date")
    private String date;

    @JsonProperty("extraction")
    private String extraction;

    public QueueMessage(String data, String operatorId, String queueName, String regionQueueName, String destination, String dateFrom, String dateTo, String hash, String group, String date, String extraction) {
        this.data = data;
        this.operatorId = operatorId;
        this.queueName = queueName;
        this.regionQueueName = regionQueueName;
        this.destination = destination;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.hash = hash;
        this.group = group;
        this.date = date;
        this.extraction = extraction;
    }

    public QueueMessage() {
        super();
    }

    @JsonProperty("data")
    public String getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(String data) {
        this.data = data;
    }

    @JsonProperty("operatorId")
    public String getOperatorId() {
        return operatorId;
    }

    @JsonProperty("operatorId")
    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    @JsonProperty("queueName")
    public String getQueueName() {
        return queueName;
    }

    @JsonProperty("queueName")
    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    @JsonProperty("receiptHandle")
    public String getReceiptHandle() {
        return receiptHandle;
    }

    @JsonProperty("receiptHandle")
    public void setReceiptHandle(String receiptHandle) {
        this.receiptHandle = receiptHandle;
    }

    @JsonProperty("regionQueueName")
    public String getRegionQueueName() {
        return regionQueueName;
    }

    @JsonProperty("regionQueueName")
    public void setRegionQueueName(String regionQueueName) {
        this.regionQueueName = regionQueueName;
    }

    @JsonProperty("forwarded")
    public boolean getForwarded() {
        return forwarded;
    }

    @JsonProperty("forwarded")
    public void setForwarded(boolean forwarded) {
        this.forwarded = forwarded;
    }

    @JsonProperty("destination")
    public String getDestination() {
        return destination;
    }

    @JsonProperty("destination")
    public void setDestination(String destination) {
        this.destination = destination;
    }

    @JsonProperty("dateFrom")
    public String getDateFrom() {
        return dateFrom;
    }

    @JsonProperty("dateFrom")
    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    @JsonProperty("dateTo")
    public String getDateTo() {
        return dateTo;
    }

    @JsonProperty("dateTo")
    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    @JsonProperty("hash")
    public String getHash() {
        return hash;
    }

    @JsonProperty("hash")
    public void setHash(String hash) {
        this.hash = hash;
    }

    @JsonProperty("group")
    public String getGroup() {
        return group;
    }

    @JsonProperty("group")
    public void setGroup(String group) {
        this.group = group;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("extraction")
    public String getExtraction() {
        return extraction;
    }

    @JsonProperty("extraction")
    public void setExtraction(String extraction) {
        this.extraction = extraction;
    }
}
