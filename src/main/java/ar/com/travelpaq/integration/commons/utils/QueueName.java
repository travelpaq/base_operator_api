package ar.com.travelpaq.integration.commons.utils;

public enum QueueName {

    STANDARDIZATION("standardization", RegionName.US_EAST_1.getRegion()),
    STANDARDIZATION_SOFTUR("standardization_softur", RegionName.US_EAST_1.getRegion()),
    STANDARDIZATION_AMICHI("standardization_amichi", RegionName.US_EAST_1.getRegion()),
    STANDARDIZATION_OLA("standardization_ola", RegionName.US_EAST_1.getRegion()),
    STANDARDIZATION_DELFOS("standardization_delfos", RegionName.US_EAST_1.getRegion()),
    STANDARDIZATION_AERO("standardization_aero", RegionName.US_EAST_1.getRegion()),
    STANDARDIZATION_JULIA("standardization_julia", RegionName.US_EAST_1.getRegion()),
    STANDARDIZATION_FREEWAY("standardization_freeway", RegionName.US_EAST_1.getRegion()),
    STANDARDIZATION_CUARTOCONTINENTE("standardization_cuartocontinente", RegionName.US_EAST_1.getRegion()),
    STANDARDIZATION_GRUPO8("standardization_grupo8", RegionName.US_EAST_1.getRegion()),
    STANDARDIZATION_TOPDEST("standardization_topdest", RegionName.US_EAST_1.getRegion()),
    STANDARDIZATION_PYTHAGORAS("standardization_pythagoras", RegionName.US_EAST_1.getRegion()),
    STANDARDIZATION_REDEVT("standardization_redevt", RegionName.US_EAST_1.getRegion()),

    STANDARDIZATION_DEV("standardization_dev", RegionName.US_EAST_1.getRegion()),
    STANDARDIZATION_DLQ("standardization_dlq", RegionName.US_EAST_1.getRegion()),

    SAVER("saver", RegionName.US_EAST_1.getRegion()),
    SAVER_DEV("saver_dev", RegionName.US_EAST_1.getRegion()),

    SAVER_DLQ("saver_dlq", RegionName.US_EAST_1.getRegion());

    private final String queueName;
    private final String queueRegion;

    QueueName(String queueName, String queueRegion) {
        this.queueName = queueName;
        this.queueRegion = queueRegion;
    }

    public String getQueueName(){ return this.queueName; }
    public String getQueueRegion(){ return  this.queueRegion; }

    public static QueueName getEnum(String value) {
        for(QueueName v : values())
            if(v.queueName.equals(value)) return v;
        throw new IllegalArgumentException();
    }
}
