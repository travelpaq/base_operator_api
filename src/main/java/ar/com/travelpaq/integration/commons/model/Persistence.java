package ar.com.travelpaq.integration.commons.model;

import ar.com.travelpaq.integration.availability.table.Packages;
import ar.com.travelpaq.integration.booking.BookingStatus;
import ar.com.travelpaq.integration.booking.model.Booking;
import ar.com.travelpaq.integration.booking.model.Passenger;
import ar.com.travelpaq.integration.booking.model.Tax;
import ar.com.travelpaq.integration.booking.table.Bookings;
import ar.com.travelpaq.integration.booking.table.Passengers;
import ar.com.travelpaq.integration.booking.table.Pricings;
import ar.com.travelpaq.integration.booking.table.Taxes;
import ar.com.travelpaq.integration.commons.configuration.ExecutionMode;
import ar.com.travelpaq.integration.commons.connection.ConnectionBookData;
import ar.com.travelpaq.integration.commons.model.domain.Package;
import org.apache.commons.codec.binary.Base64;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Persistence {
    static private String db_endpoint = System.getenv("DB_ENDPOINT");
    static private String db_pwd = System.getenv("DB_PWD");
    static private String db_username = System.getenv("DB_USERNAME");
    static private String dev_db_endpoint = db_endpoint.replace("://packages.","://packages-dev.");

    public static Boolean updateAvail(Package _package, ExecutionMode executionMode) throws Exception {

        byte[] valueDecoded= Base64.decodeBase64(_package.getId());
        String str = new String(valueDecoded);

        Session session = getConfigurationHibernet(executionMode, Packages.class).buildSessionFactory().openSession();

        Packages packages = new Packages();
        packages.setModified(new Date());
        packages.setSeatsAvailable(_package.getAvail().getSeats());
        packages.setRoomsAvailable(_package.getAvail().getRooms());

        if (_package.getAvail().getRooms() == 0 || _package.getAvail().getSeats() == 0) {
            packages.setDeleted(1);
        } else {
            packages.setDeleted(0);
        }
        
        packages.setId(Integer.parseInt(str.substring(1, str.indexOf(","))));
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(packages);
            session.flush();
            session.clear();
            transaction.commit();
            return true;
        }catch(HibernateException hex){
            return false;
        }
    }

    public static int saveBookingEmpty(Booking booking, ExecutionMode executionMode) throws ParseException {

        Integer id=0;
        List<Class> clases = new ArrayList<>();
        clases.add(Bookings.class);
        clases.add(Pricings.class);
        clases.add(Taxes.class);
        clases.add(Passengers.class);
        Session session = getConfigurationHibernet(executionMode, clases).buildSessionFactory().openSession();

        Bookings tableBookings = new Bookings();
        tableBookings.setOperator_id(booking.get_package().getCompany().getId());
        tableBookings.setAgency_id(booking.getAgencyId());
        tableBookings.setStatus(BookingStatus.CREATING);
        tableBookings.setCreated(new Date());
        tableBookings.setMarkup(booking.get_package().getPrice().getMarkup());
        tableBookings.setType_change(booking.get_package().getPrice().getChange());
        tableBookings.setPackage_id(booking.getPackageId());
        tableBookings.setAgency_comission(booking.get_package().getPrice().getAgencyCommission());
        tableBookings.setContact_phone(booking.getContactPhone());
        tableBookings.setLabel(booking.get_package().getPrice().getLabel());

        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(tableBookings);
            id = tableBookings.getId();
            session.flush();
            session.clear();
            transaction.commit();
            return id;
        }catch(HibernateException hex){
            return id;
        }
    }

    public static Boolean saveBooking(Booking booking, ExecutionMode executionMode, ConnectionBookData connectionBookData) {

        List<Class> clases = new ArrayList<>();
        clases.add(Bookings.class);
        clases.add(Pricings.class);
        clases.add(Taxes.class);
        clases.add(Passengers.class);
        Session session = getConfigurationHibernet(executionMode, clases).buildSessionFactory().openSession();

        Bookings tableBookings = new Bookings();
        tableBookings.setId(booking.getId());
        tableBookings.setExternal_id(booking.getExternalId());
        tableBookings.setStatus(booking.getStatus());
        tableBookings.setPercentage_tp_operator(connectionBookData.getPercentage_tp_operator());
        tableBookings.setPercentage_tp_agency(connectionBookData.getPercentage_tp_ota());

        Pricings tablePricings = new Pricings(booking.getPricing().getTotal(), booking.getPricing().getCommissionAmount(),
                booking.getPricing().getCommissionablePrice(), new Date(), tableBookings, null);

        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(tableBookings);
            session.save(tablePricings);

            for(Tax t : booking.getPricing().getFiscalTaxes()){
                Taxes txs = new Taxes(t.getRate(),t.getBase(),t.getAmount(),t.getDescription(),
                        "Fiscal Taxes",new Date(), tablePricings);
                session.save(txs);
            }

            for(Tax t : booking.getPricing().getNonCommissionableService()){
                Taxes txs = new Taxes(t.getRate(),t.getBase(),t.getAmount(),t.getDescription(),
                        "Non Commissionable Taxes",new Date(), tablePricings);
                session.save(txs);
            }

            for(Tax t : booking.getPricing().getTourismTaxes()){
                Taxes txs = new Taxes(t.getRate(),t.getBase(),t.getAmount(),t.getDescription(),
                        "Tourism Taxes",new Date(), tablePricings);
                session.save(txs);
            }

            int room = 1;
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            for(List<Passenger> lista : booking.getRooms()){
                room++;
                for(Passenger p : lista){
                    Passengers pax;
                    Date passportExpirationDate = null;
                    if(p.getExpirationPassportDate() != null && !p.getExpirationPassportDate().isEmpty()){
                        try {
                            passportExpirationDate = df.parse(p.getExpirationPassportDate());
                        } catch (Exception e){
                            e.printStackTrace();
                            passportExpirationDate = null;
                        }
                    }
                    pax = new Passengers(p.getName(), p.getSurname(), p.getKindDoc(), p.getNumDoc(),
                                p.getGender(), df.parse(p.getBirthdate()),p.getResidence(), p.getNationality(), p.getMail(),
                                passportExpirationDate, new Date(), room, tableBookings);
                    session.save(pax);
                }
            }

            session.flush();
            session.clear();
            transaction.commit();
            return true;
        }catch(HibernateException | ParseException hex){
            return false;
        }
    }

    public static Boolean updateBooking(Booking booking, ExecutionMode executionMode, ConnectionBookData connectionBookData) {

        List<Class> clases = new ArrayList<>();
        clases.add(Bookings.class);
        clases.add(Pricings.class);
        clases.add(Taxes.class);
        clases.add(Passengers.class);
        Session session = getConfigurationHibernet(executionMode, clases).buildSessionFactory().openSession();

        Bookings tableBookings = new Bookings();
        tableBookings.setId(booking.getId());
        tableBookings.setStatus(booking.getStatus());
        tableBookings.setExternal_id(booking.getExternalId());
        tableBookings.setPercentage_tp_agency(connectionBookData.getPercentage_tp_ota());
        tableBookings.setPercentage_tp_operator(connectionBookData.getPercentage_tp_operator());

        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(tableBookings);
            session.flush();
            session.clear();
            transaction.commit();
            return true;
        }catch(HibernateException hex){
            return false;
        }
    }

    public static org.hibernate.cfg.Configuration getConfigurationHibernet(ExecutionMode executionMode, Class annotatedClass){
        //Create configuration for the conection with de database
        org.hibernate.cfg.Configuration configuration = new org.hibernate.cfg.Configuration();
        configuration.addAnnotatedClass (annotatedClass);

        configuration.setProperty("hibernate.bytecode.use_reflection_optimizer", "true");
        configuration.setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");

        if (executionMode == ExecutionMode.PROD)
            configuration.setProperty("hibernate.connection.url", db_endpoint);
        else
            configuration.setProperty("hibernate.connection.url", dev_db_endpoint);

        configuration.setProperty("hibernate.connection.username", db_username);
        configuration.setProperty("hibernate.connection.password", db_pwd);

        configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        configuration.setProperty("hibernate.search.autoregister_listeners", "false");
        configuration.setProperty("hibernate.cache.use_second_level_cache", "false");
        configuration.setProperty("hibernate.jdbc.batch_size", "50");
        configuration.setProperty("hibernate.order_inserts", "true");
        configuration.setProperty("hibernate.jdbc.batch_versioned_data", "true");
        configuration.setProperty("hibernate.validator.apply_to_ddl", "false");
        return configuration;
    }

    public static org.hibernate.cfg.Configuration getConfigurationHibernet(ExecutionMode executionMode, List<Class> annotatedClass){
        //Create configuration for the conection with de database
        org.hibernate.cfg.Configuration configuration = new org.hibernate.cfg.Configuration();
        for(Class clazz : annotatedClass){
            configuration.addAnnotatedClass(clazz);
        }


        configuration.setProperty("hibernate.bytecode.use_reflection_optimizer", "true");
        configuration.setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");

        if (executionMode == ExecutionMode.PROD)
            configuration.setProperty("hibernate.connection.url", db_endpoint);
        else
            configuration.setProperty("hibernate.connection.url", dev_db_endpoint);

        configuration.setProperty("hibernate.connection.username", db_username);
        configuration.setProperty("hibernate.connection.password", db_pwd);

        configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        configuration.setProperty("hibernate.search.autoregister_listeners", "false");
        configuration.setProperty("hibernate.cache.use_second_level_cache", "false");
        configuration.setProperty("hibernate.jdbc.batch_size", "50");
        configuration.setProperty("hibernate.order_inserts", "true");
        configuration.setProperty("hibernate.jdbc.batch_versioned_data", "true");
        configuration.setProperty("hibernate.validator.apply_to_ddl", "false");
        return configuration;
    }
}
